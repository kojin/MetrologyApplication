#pragma once
#include <iostream>

class TurtleSwitch
{
private:
	int tid;
	bool ison;
public:
	TurtleSwitch() {};
	TurtleSwitch(int _tid);
	virtual ~TurtleSwitch();
	void setid(int _tid);
	int Open();
	int Close();
	int onofftest();
	int takepicture();

	bool isOn() { return ison; }
};

