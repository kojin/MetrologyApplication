#include "stdafx.h"
#include <sstream>
#include "Rs232cRVCtrl.h"


Rs232cRVCtrl::Rs232cRVCtrl()
{
	m_prsMain = NULL;
}
Rs232cRVCtrl::~Rs232cRVCtrl()
{
}



// -----------------------------------------------------------------
// 		RS�232C Initialize
// -----------------------------------------------------------------
int Rs232cRVCtrl::RS_Setup(int m_nPort)
{
	if (m_prsMain)return 0;
	m_prsMain = new CRs232c;


	std::stringstream ss; ss.str("");
//	ss << "COM" << m_nPort << "";
	ss << "\\\\.\\COM" << m_nPort << "";

	if (!m_prsMain->OpenCommPort((char*)ss.str().c_str(), 2049, 2049)) return 1;

//	INIT_COMM_PAC pac;
//	pac.BaudRate = 9600;	// 9600bout
////	pac.BaudRate = 96;	// 96bout
//	pac.ByteSize = 8;		// 8bits
//	pac.Parity = 0;		// non parity
//	pac.StopBits = 1;		// 1 stop bit

	DCB dcb;
	COMMTIMEOUTS ct;
	DWORD			dwError;

	/*Get Comm Port Status*/
	if (!GetCommState(*m_prsMain->GetHComm(), &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : GetCommState() Error", dwError);
		return(FALSE);
	}

	/* Build Comm DCB */
	//	if (!BuildCommDCBA("96,n,8,2,x",&dcb)) {
	if (FALSE == BuildCommDCBA("baud=19200 parity=n data=8 stop=1 xon=on", &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : BuildCommDCB() Error", dwError);
		return(FALSE);
	}

	/* Get TimeoutSetting */
	if (!GetCommTimeouts(*m_prsMain->GetHComm(), &ct)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : GetCommTimeouts() Error", dwError);
		return(FALSE);

	}
	/* Set COMMTIOMEOUT Structure */
	ct.ReadIntervalTimeout = 5;
	ct.WriteTotalTimeoutMultiplier = 5;
	ct.WriteTotalTimeoutConstant = 50;


	if (!m_prsMain->InitCommPort2())return 3;		// Parameter failed

	return 0;
}
void Rs232cRVCtrl::RS_Reset(void)
{
	if (m_prsMain) {
		m_prsMain->CloseCommPort();
		delete m_prsMain;
	}
	m_prsMain = NULL;
}
// -----------------------------------------------------------------
// 		Send AF ( String )
// -----------------------------------------------------------------
void Rs232cRVCtrl::WriteStringMsc(char *cCmd)
{

	int n = strlen(cCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(cCmd, n);
	}
}

// -----------------------------------------------------------------
// 		Recieve AF
// -----------------------------------------------------------------
int Rs232cRVCtrl::ReadMsc(char *m_strResp, unsigned int pt)
{
	int		rc;
	char	sDummy[2];

	if (m_prsMain) {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = 0;
		m_prsMain->ReadCommBlock(m_strResp, pt);	// Read
		while (!m_prsMain->ReadComm(1, sDummy));
	}
	else {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = '0';
	}
	return 0;
}
int Rs232cRVCtrl::ReadMsc(char *m_strResp)
{
	int pt = 1;
	int		rc;
	char	sDummy[2];

	if (m_prsMain) {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = 0;
		char m_tmpdata[1];
		m_tmpdata[0]= NULL;
		while (m_tmpdata[0] != CR) {
			m_prsMain->ReadCommBlock(m_tmpdata, 1);	// Read
			sprintf_s(m_strResp+ strlen(m_strResp), sizeof(m_strResp), "%c", m_tmpdata[0]);
		}
	}
	else {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = '0';
	}
	int nlen = strlen(m_strResp);//	m_strResp
	std::cout << nlen << " " << m_strResp << std::endl;

	return 0;
}
// -----------------------------------------------------------------
// 		Send AF
// -----------------------------------------------------------------
void Rs232cRVCtrl::WriteMsc(void)
{

	int n = strlen(m_strCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(m_strCmd, n);
	}
}

int Rs232cRVCtrl::RVSet(int zoom) {
	int add = 0;
	if (zoom == 2) {
		add = 1;
	}
	else if (zoom == 5)
	{
		add = 2;
	}
	else if (zoom == 10) {
		add = 3;
	}
	else {
		std::cout << "strange zoom : " << zoom << " has been set..." << std::endl;
		return -1;
	}

	strncpy_s(m_strCmd, sizeof(m_strCmd), "cRDC", 4);
	sprintf_s(m_strCmd + 4, sizeof(m_strCmd), "%d%c",add, CR);

	std::cout << "sending command " << (char*)m_strCmd << std::endl;
	WriteMsc();
	return 0;
}

int Rs232cRVCtrl::RVTest() {
	int nRcnt, nlen;

	strncpy_s(m_strCmd, sizeof(m_strCmd), "cRCR", 4);
	sprintf_s(m_strCmd + 4, sizeof(m_strCmd), "%c",CR);

	std::cout << "sending command " << (char*)m_strCmd << std::endl;
	WriteMsc();
	Sleep(500);
	ReadMsc(m_strResp);
	if (strlen(m_strResp) != 5 ) {
		std::cout << "command " << m_strCmd << " not completed..." << std::endl;
		return -1;
	}

	strncpy_s(m_strCmd, sizeof(m_strCmd), "cRCW", 4);
	sprintf_s(m_strCmd + 4, sizeof(m_strCmd), "%c", CR);

	std::cout << "sending command " << (char*)m_strCmd << std::endl;
	WriteMsc();
	Sleep(500);
	ReadMsc(m_strResp);
	if (strlen(m_strResp) != 5) {
		std::cout << "command " << m_strCmd << " not completed..." << std::endl;
		return -1;
	}

	strncpy_s(m_strCmd, sizeof(m_strCmd), "rRAR", 4);
	sprintf_s(m_strCmd + 4, sizeof(m_strCmd), "%c", CR);
	std::cout << "sending command " << (char*)m_strCmd << std::endl;
	WriteMsc();
	Sleep(500);
	ReadMsc(m_strResp);
	if (strlen(m_strResp) != 6) {
		std::cout << "command " << m_strCmd << " not completed..." << std::endl;
		return -1;
	}
	else {
		std::cout << "address : " << m_strResp[4]  << std::endl;
	}
	//	if ((nRcnt = ReadMsc(m_strResp, 5)) != 0) return -1;
//	nRcnt=m_prsMain->ReadCommBlock(m_strResp, 1);
//	nlen = strlen(m_strResp);//	m_strResp
//	std::cout << nlen << " " << m_strResp << std::endl;

	return 0;
}