#include "stdafx.h"
#include "Rs232cWLCtrl.h"
#include <sstream>

Rs232cWLCtrl::Rs232cWLCtrl()
{
	m_prsMain = NULL;
	eh = new WLErrorHandler();

}
Rs232cWLCtrl::~Rs232cWLCtrl()
{
}
int Rs232cWLCtrl::RS_Setup(int m_nPort)
{
	std::cout << "starting RS_Setup" << std::endl;
	if (m_prsMain)return 0;
	std::cout << " initialize m_prsMain" << std::endl;
	m_prsMain = new CRs232c;


	std::stringstream ss; ss.str("");
	ss << "\\\\.\\COM" << m_nPort << "";
	if (!m_prsMain->OpenCommPort((char*)ss.str().c_str(), 2049, 2049)) return 1;

	if (!m_prsMain->InitCommPort("baud=38400 parity=n data=8 stop=1 xon=off"))return 3;		// Parameter failed

	for (int ii = 0; ii < 25; ii++) m_TS[ii] = 2;  // Tray Status 0: tray empty  1: tray exist  2: unknown 3: measured 
//	for (int ii = 0; ii < 3; ii++) m_ORGS[ii] = 2;
	m_ORGS = 2;
	m_CS = 2;      // cassete status 0: tray empty  1: tray exist  2: unknown
	m_arm = 2;     // 0: empty 1: wafer exist  2: unknown
	m_aligner = 2; // 0: empty 1: wafer exist  2: unknown
	m_XYstage = 2; // 0: empty 1: wafer exist  2: unknown
	return 0;
}

void Rs232cWLCtrl::RS_Reset(void)
{
	if (m_prsMain) {
		m_prsMain->CloseCommPort();
		delete m_prsMain;
	}
	m_prsMain = NULL;
}
// -----------------------------------------------------------------
// 		Send cmd ( String )
// -----------------------------------------------------------------
void Rs232cWLCtrl::WriteStringMsc(char *cCmd)
{

	int n = strlen(cCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(cCmd, n);
	}
}

// -----------------------------------------------------------------
// 		Recieve WL
// -----------------------------------------------------------------
int Rs232cWLCtrl::ReadMsc(char *m_strResp, unsigned int pt)
{
	int		rc;
	char	sDummy[2];

	if (m_prsMain) {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = 0;
		m_prsMain->ReadCommBlock(m_strResp, pt);	// Read
		while (!m_prsMain->ReadComm(1, sDummy));
	}
	else {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = '0';
	}
	return 0;
}
int Rs232cWLCtrl::ReadMsc(char *m_strResp)
{
	int pt = 1;
	int		rc;
	char	sDummy[2];

	if (m_prsMain) {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = 0;
		char m_tmpdata[1];
		m_tmpdata[0] = NULL;
		while (m_tmpdata[0] != CR) {
			if (m_tmpdata[0] = NULL)Sleep(500);
			m_prsMain->ReadCommBlock(m_tmpdata, 1);	// Read
//			std::cout << m_tmpdata << std::endl;
			sprintf_s(m_strResp + strlen(m_strResp), sizeof(m_strResp), "%c", m_tmpdata[0]);
		}
	}
	else {
		for (rc = 0; rc < 50; rc++) *(m_strResp + rc) = '0';
	}
	int nlen = strlen(m_strResp);//	m_strResp
//	std::cout << nlen << " " << m_strResp << std::endl;

	return 0;
}
void Rs232cWLCtrl::WriteMsc(void)
{

	int n = strlen(m_strCmd);
	if (m_prsMain) {
		m_prsMain->WriteCommBlock(m_strCmd, n);
	}
}
int Rs232cWLCtrl::SendReceiveCommand(std::string cmd, int iwait=500,bool print=true) {
	if(print)std::cout << "Sending " << cmd << " command" << std::endl;
	strncpy_s(m_strCmd, sizeof(cmd.c_str())+100, cmd.c_str(), strlen(cmd.c_str()));
	sprintf_s(m_strCmd + strlen(cmd.c_str()), sizeof(m_strCmd), "%c", CR);
	WriteMsc();
	Sleep(iwait);
	ReadMsc(m_strResp);
	std::string m_strRespStr = m_strResp;
	if(print)std::cout << "returned " << m_strRespStr << std::endl;

	std::string maincmd="";
	std::string::size_type pos = cmd.find(",");
	if (pos != std::string::npos) maincmd = cmd.substr(0,pos);
	else maincmd = cmd;
//	std::cout << maincmd << std::endl;
	if (maincmd == "UNITINFO" || maincmd == "CARRIERSTS" || maincmd == "VACINFO" || maincmd == "VACON" || maincmd == "VACOFF" || maincmd == "ORGINFO" || maincmd == "SLOTMAPINFO")return 0;

	if(print) std::cout << m_strRespStr.substr(maincmd.length() + 1, 2) << std::endl;
	if (m_strRespStr.substr(maincmd.length() + 1, 2)=="OK")return 0;
	else {
		std::cout << "Command " << cmd << " Receive Error " << std::endl;
		std::cout << "     Err : " << m_strRespStr << std::endl;
		return -1; 
	}

}
std::string Rs232cWLCtrl::GetError() {
	m_strResp[strlen(m_strResp) - 1] = NULL;
	std::string m_strRespStr = m_strResp;
	std::cout << "#" << m_strRespStr << "#" << std::endl;

	std::string errornum= m_strRespStr.substr(m_strRespStr.find_last_of(",")+1);
	
	WLError wle = eh->GetWLError(errornum);
	std::cout << wle.shorterror << std::endl;
	std::stringstream sserr; sserr.str("");
	sserr << "Command " << m_strCmd << " Receive Error \n"
		<< "Receive " << m_strResp << "\n"
		<< "     �G���[(" << errornum << ") : " << wle.shorterror << "\n"
		<< "     " << wle.error << "\n"
		<< "     " << wle.action;
	std::cout << wle.error << std::endl;
	std::cout << sserr.str() << std::endl;
	return sserr.str();
}
int Rs232cWLCtrl::CheckStatus(bool domapping, bool print) {
//	for (int ii = 0; ii < 3; ii++) m_ORGS[ii] = 2;

	if (SendReceiveCommand("VACINFO", 500, print) != 0)return -1; // --> "ORGINFO,1,1,1"
	std::cout << "ORGINFO : " << atoi(&m_strResp[8]) << " " << atoi(&m_strResp[10]) << " " << atoi(&m_strResp[12]) << std::endl;
	int _m_VACS= m_strResp[12] - '0';

	if (SendReceiveCommand("ORGINFO", 500, print) != 0)return -1; // --> "ORGINFO,1,1,1"
	std::cout << "ORGINFO : " << atoi(&m_strResp[8]) << " " << atoi(&m_strResp[10]) << " " << atoi(&m_strResp[12]) << std::endl;
	//	std::string aa = m_strResp[8];
	int _m_ORGS = m_strResp[8] - '0';
//	m_ORGS[1] = atoi(&m_strResp[10]);
//	m_ORGS[2] = atoi(&m_strResp[12]);
	int _m_TS[25];
	for (int ii = 0; ii < 25; ii++) _m_TS[ii] = 2;  // Tray Status 0: tray empty  1: tray exist  2: measuring 3: measured 4: unknown
	if (SendReceiveCommand("SLOTMAPINFO,1", 500, print) != 0)return -1; // --> "SLOTMAPINFO,1,6,1111111111111110000000000, 5555555555555554444444444"
	for (int ii = 42; ii < 67; ii++) {
		char cc = m_strResp[ii];
		int trstat = atoi(&cc);
		if(trstat==2||trstat==4) _m_TS[ii - 42] = 0;
		else if (trstat >= 5&&trstat<=7) _m_TS[ii - 42] = trstat-4;
		else _m_TS[ii - 42] = 4;
	}


	if(SendReceiveCommand("CARRIERSTS,1",500,print) != 0)return -1; // --> "CARRIERSTS,1,1"
	m_CS = atoi(&m_strResp[13]);
	if(SendReceiveCommand("UNITINFO", 500, print) != 0)return -1; // --> "UNITINFO,1,0,1"
	m_arm = atoi(&m_strResp[9]); 
	m_aligner = atoi(&m_strResp[11]);
	m_XYstage = atoi(&m_strResp[13]);

	if (domapping) {
		for (int ii = 0; ii < 25; ii++) _m_TS[ii] = 2;  // Tray Status 0: tray empty  1: tray exist  2: unknown 3: measured 

		if(SendReceiveCommand("MAPPING,1", 500, print) != 0)return -1; // --> MAPPING OK,1,6,1111111111111110000000000
		for (int ii = 15; ii < 40; ii++) {
			char cc = m_strResp[ii];
			_m_TS[ii - 15] = atoi(&cc);
		}
	}

	m_ORGS = _m_ORGS;
	m_VACS = _m_VACS;
	for (int ii = 0; ii < 25; ii++) m_TS[ii] = _m_TS[ii];  // Tray Status 0: tray empty  1: tray exist  2: measuring 3: measured 4: unknown
	if (print) {
		std::cout << "ORIG : " << m_ORGS << std::endl;
		std::cout << "VAC : " << m_VACS << std::endl;
		std::cout << "Casset : " << m_CS << std::endl;
		std::cout << "Wafer is : " << m_arm << " " << m_aligner << " " << m_XYstage << std::endl;
		std::cout << "tray status : ";
		for (int ii = 0; ii < 25; ii++) {
			std::cout << m_TS[ii];
		}
		std::cout << std::endl;
	}


	return 0;
}
int Rs232cWLCtrl::Initialization() {
	std::cout << "Go to Hope position : " << std::endl;
	if(SendReceiveCommand("INIT") != 0)return -1;
	if (SendReceiveCommand("READY") != 0)return -1;

	if (CheckStatus(false, true) != 0)return -1;
	return 0;
}
int Rs232cWLCtrl::doMapping() {
	if (m_CS != 1) {
		std::cout << "please initialize Loader first." << std::endl;
		return -1;
	}
	std::cout << "Mapping... " << std::endl;
	SendReceiveCommand("READY");
//	SendReceiveCommand("INIT");
	CheckStatus(true, true);
	std::cout << "Mapping done " << std::endl;
	return 0;
}

int Rs232cWLCtrl::WaferLoaderTest() {
	return 0;
	SendReceiveCommand("READY");
	SendReceiveCommand("INIT");
	SendReceiveCommand("CARRIERSTS,1");
	SendReceiveCommand("UNITINFO");
	SendReceiveCommand("MAPPING,1");
	SendReceiveCommand("LOADINFO,1,1010000000000000000000000,001",5000);
	SendReceiveCommand("LOTSTART");
	SendReceiveCommand("LOAD,1,1");
	SendReceiveCommand("UNITINFO");
	SendReceiveCommand("UNLOAD,4,1,1");
	SendReceiveCommand("LOTEND");


	return 0;
}