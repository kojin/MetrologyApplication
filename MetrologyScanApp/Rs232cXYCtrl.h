#pragma once
#include <iostream>
#include	"Rs232c.h"

#define		LF			0x0a
#define		CR			0x0d
#define		NUL			0x00
#define		ErrRetry	3000


class Rs232cXYCtrl
{
public:
	Rs232cXYCtrl();
	~Rs232cXYCtrl();

protected:
	DCB		dcb;								// RS-232C Port
	COMSTAT	ComStat;
	int		IdCom, comerr;
	char	m_strCmd[20];                       // Send Character buffer

	void 	WriteMsc(void);					// Send MSC

public:
	char	m_strResp[50];                      // Recive character buffer
	char	m_strDummy[5];                      // Recive character buffer
	CRs232c *m_prsMain;							// RS-232C Class
	int		RS_Setup(int m_nPort);			// RS Initialize
	void	RS_Reset(void);					// RS Close
	int		Origin(int nAxis);				// Orogin
	int		Absolute(int nAxis, float fval);	// ABS Move
	int		Absolute2D(float fvalx, float fvaly);	// ABS Move 2 dimention
	int     GoHome();
	int		RelativeP(int nAxis, float fval);	// REL PLUS Move
	int		RelativeM(int nAxis, float fval);	// REL MINUS Move
	int		MscSts(int nAxis);				// Status Set
	int		MscSensor(void);					// Sensor status
	int		MscPos(void);						// Current position
	int		ReadMsc(char *m_strResp, unsigned int pt);
	void	WriteStringMsc(char *cCmd);		// Send string MSC
	void    PrintPosition();

	float	m_fAValue;							// A Axis Current;
	float	m_fBValue;							// B Axis Current;
	BOOL	m_bBusy;							// Busy Flag

	BOOL	m_bALimitPlus;						// A Axis +limit sensor
	BOOL	m_bALimitMinus;						// A Axis -limit sensor
	BOOL	m_bABeforeOrigin;					// A Axis before origin sensor
	BOOL	m_bAOrigin;							// A Axis origin sensor
	BOOL	m_bBLimitPlus;						// A Axis +limit sensor
	BOOL	m_bBLimitMinus;						// A Axis -limit sensor
	BOOL	m_bBBeforeOrigin;					// A Axis before origin sensor
	BOOL	m_bBOrigin;							// A Axis origin sensor
};

