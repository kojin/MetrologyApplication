#pragma once
#include "MetrologyScan.h"
#include "CoordinateSystem.h"
#include "ScanMarker.h"
#include "windows.h"
//#include "number.h"
#include <iostream>
#include <sstream>
#include <string>
#include <msclr/marshal_cppstd.h>

#define WaferLoaderOnly 0

namespace MetrologyScanApp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;

	/// <summary>
	/// ControlGUI の概要
	/// </summary>
	public ref class ControlGUI : public System::Windows::Forms::Form
	{
	public:
		ControlGUI(void)
		{
			InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します
			//
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~ControlGUI()
		{
			if (components)
			{
				delete components;
			}
		}
	private: MetrologyScan ms0;
	private: Bitmap^ ScanPositionCanvas;
	private: MoveScanMarker::ScanMarker^ scanmark;
	private: double nowTime = 0;
	private: bool scanRunning = false;
	private: bool scanfinished = false;
	private: bool wlinitfinished = false;
	private: bool wlmappingfinished = false;
	private: bool emergencystop = false;
	private: bool AFlock = true;
	private: Thread^ tscan;
	private: Thread^ tsc0;
	private: int zpos;
	private: float zposum;
	private: int traynum;
	private: System::String^ lastpicname;
	private: System::String^ snstring;
	private: Bitmap^ canvas;
	private: Graphics^ g;
	private: Image^ img;

	private: System::Windows::Forms::Button^  startBotton;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::TextBox^  startx;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  starty;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  stepx;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::TextBox^  stepy;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::TextBox^  endx;
	private: System::Windows::Forms::TextBox^  endy;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::PictureBox^  ScanPositionViewer;

	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Label^ label13;


	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::TextBox^  textTimer;
	private: System::Windows::Forms::Label^  label16;

	private: System::Windows::Forms::Button^  stopButton;
	private: System::Windows::Forms::GroupBox^  groupBox1;

	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::TextBox^  AFposView;

	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::RadioButton^  rbx2;

	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::RadioButton^  rbx10;

	private: System::Windows::Forms::RadioButton^  rbx5;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::TextBox^  stepval;

	private: System::Windows::Forms::Button^  stageright;

	private: System::Windows::Forms::Button^  stageleft;
	private: System::Windows::Forms::Button^  stagedown;


	private: System::Windows::Forms::Button^  stageup;

	private: System::Windows::Forms::Label^  label19;
	private: System::Windows::Forms::Button^  stageorig;
	private: System::Windows::Forms::GroupBox^  groupBox5;
	private: System::Windows::Forms::CheckBox^  picture;
	private: System::Windows::Forms::CheckBox^  afocus;
	private: System::Windows::Forms::Button^  singlescan;
	private: System::Windows::Forms::Button^  movenear;
	private: System::Windows::Forms::Button^  movefar;
	private: System::Windows::Forms::CheckBox^  closeplot;
	private: System::Windows::Forms::TextBox^  serialnumber;
	private: System::Windows::Forms::Label^  label20;
private: System::Windows::Forms::Label^  label21;
private: System::Windows::Forms::TextBox^  textBox1;
private: System::Windows::Forms::Button^  TakeWafer;
private: System::Windows::Forms::GroupBox^  groupBox6;
private: System::Windows::Forms::GroupBox^  groupBox7;
private: System::Windows::Forms::MenuStrip^  menuStrip1;
private: System::Windows::Forms::Button^  button4;

//private: cli::array<System::Windows::Forms::Label^>^ tray;
		 
private: System::Windows::Forms::Label^  tray1;
private: System::Windows::Forms::Label^  tray2;
private: System::Windows::Forms::Label^  tray4;
private: System::Windows::Forms::Label^  tray3;
private: System::Windows::Forms::Label^  tray8;
private: System::Windows::Forms::Label^  tray7;
private: System::Windows::Forms::Label^  tray6;
private: System::Windows::Forms::Label^  tray5;
private: System::Windows::Forms::Label^  tray13;
private: System::Windows::Forms::Label^  tray12;
private: System::Windows::Forms::Label^  tray11;
private: System::Windows::Forms::Label^  tray10;
private: System::Windows::Forms::Label^  tray9;
private: System::Windows::Forms::Label^  tray25;
private: System::Windows::Forms::Label^  tray24;
private: System::Windows::Forms::Label^  tray23;
private: System::Windows::Forms::Label^  tray22;
private: System::Windows::Forms::Label^  tray21;
private: System::Windows::Forms::Label^  tray20;
private: System::Windows::Forms::Label^  tray19;
private: System::Windows::Forms::Label^  tray18;
private: System::Windows::Forms::Label^  tray17;
private: System::Windows::Forms::Label^  tray16;
private: System::Windows::Forms::Label^  tray15;
private: System::Windows::Forms::Label^  tray14;
		 
private: System::Windows::Forms::Label^  cassette;


private: System::Windows::Forms::Button^  WLcheck;

private: System::Windows::Forms::GroupBox^  groupBox8;
private: System::Windows::Forms::Button^  WLinit;
private: System::Windows::Forms::Button^  WLmapping;
private: System::Windows::Forms::Button^  load;
private: System::Windows::Forms::ComboBox^  combsetTray;
private: System::Windows::Forms::Button^ unload;
private: System::Windows::Forms::Button^ vacon;
private: System::Windows::Forms::Button^ vacoff;

private: System::Windows::Forms::PictureBox^  pictureBox4;
private: System::Windows::Forms::PictureBox^  pictureBox3;
private: System::Windows::Forms::PictureBox^  pictureBox2;
private: System::Windows::Forms::ComboBox^  comboBox2;
private: System::Windows::Forms::PictureBox^ picjusttaken;
private: System::Windows::Forms::Label^ label23;
private: System::Windows::Forms::TextBox^ textBox2;
private: System::Windows::Forms::Label^ label22;
private: System::Windows::Forms::Label^ OrigInit;











	private: System::ComponentModel::IContainer^  components;


	protected:

	private:
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(ControlGUI::typeid));
			this->startBotton = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->startx = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->starty = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->stepx = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->stepy = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->endx = (gcnew System::Windows::Forms::TextBox());
			this->endy = (gcnew System::Windows::Forms::TextBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->ScanPositionViewer = (gcnew System::Windows::Forms::PictureBox());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->textTimer = (gcnew System::Windows::Forms::TextBox());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->stopButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->singlescan = (gcnew System::Windows::Forms::Button());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->rbx10 = (gcnew System::Windows::Forms::RadioButton());
			this->rbx5 = (gcnew System::Windows::Forms::RadioButton());
			this->rbx2 = (gcnew System::Windows::Forms::RadioButton());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->stageorig = (gcnew System::Windows::Forms::Button());
			this->stageright = (gcnew System::Windows::Forms::Button());
			this->TakeWafer = (gcnew System::Windows::Forms::Button());
			this->stageleft = (gcnew System::Windows::Forms::Button());
			this->stagedown = (gcnew System::Windows::Forms::Button());
			this->stageup = (gcnew System::Windows::Forms::Button());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->stepval = (gcnew System::Windows::Forms::TextBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->movenear = (gcnew System::Windows::Forms::Button());
			this->movefar = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->AFposView = (gcnew System::Windows::Forms::TextBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->picture = (gcnew System::Windows::Forms::CheckBox());
			this->afocus = (gcnew System::Windows::Forms::CheckBox());
			this->closeplot = (gcnew System::Windows::Forms::CheckBox());
			this->serialnumber = (gcnew System::Windows::Forms::TextBox());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->unload = (gcnew System::Windows::Forms::Button());
			this->vacon = (gcnew System::Windows::Forms::Button());
			this->vacoff = (gcnew System::Windows::Forms::Button());
			this->combsetTray = (gcnew System::Windows::Forms::ComboBox());
			this->load = (gcnew System::Windows::Forms::Button());
			this->WLcheck = (gcnew System::Windows::Forms::Button());
			this->WLmapping = (gcnew System::Windows::Forms::Button());
			this->WLinit = (gcnew System::Windows::Forms::Button());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->tray1 = (gcnew System::Windows::Forms::Label());
			this->tray2 = (gcnew System::Windows::Forms::Label());
			this->tray4 = (gcnew System::Windows::Forms::Label());
			this->tray3 = (gcnew System::Windows::Forms::Label());
			this->tray8 = (gcnew System::Windows::Forms::Label());
			this->tray7 = (gcnew System::Windows::Forms::Label());
			this->tray6 = (gcnew System::Windows::Forms::Label());
			this->tray5 = (gcnew System::Windows::Forms::Label());
			this->tray13 = (gcnew System::Windows::Forms::Label());
			this->tray12 = (gcnew System::Windows::Forms::Label());
			this->tray11 = (gcnew System::Windows::Forms::Label());
			this->tray10 = (gcnew System::Windows::Forms::Label());
			this->tray9 = (gcnew System::Windows::Forms::Label());
			this->tray25 = (gcnew System::Windows::Forms::Label());
			this->tray24 = (gcnew System::Windows::Forms::Label());
			this->tray23 = (gcnew System::Windows::Forms::Label());
			this->tray22 = (gcnew System::Windows::Forms::Label());
			this->tray21 = (gcnew System::Windows::Forms::Label());
			this->tray20 = (gcnew System::Windows::Forms::Label());
			this->tray19 = (gcnew System::Windows::Forms::Label());
			this->tray18 = (gcnew System::Windows::Forms::Label());
			this->tray17 = (gcnew System::Windows::Forms::Label());
			this->tray16 = (gcnew System::Windows::Forms::Label());
			this->tray15 = (gcnew System::Windows::Forms::Label());
			this->tray14 = (gcnew System::Windows::Forms::Label());
			this->cassette = (gcnew System::Windows::Forms::Label());
			this->groupBox8 = (gcnew System::Windows::Forms::GroupBox());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->picjusttaken = (gcnew System::Windows::Forms::PictureBox());
			this->OrigInit = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ScanPositionViewer))->BeginInit();
			this->groupBox1->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox8->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picjusttaken))->BeginInit();
			this->SuspendLayout();
			// 
			// startBotton
			// 
			this->startBotton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"startBotton.Image")));
			this->startBotton->Location = System::Drawing::Point(363, 150);
			this->startBotton->Name = L"startBotton";
			this->startBotton->Size = System::Drawing::Size(114, 118);
			this->startBotton->TabIndex = 0;
			this->startBotton->UseVisualStyleBackColor = true;
			this->startBotton->Click += gcnew System::EventHandler(this, &ControlGUI::startBotton_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(12, 9);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(261, 120);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			// 
			// startx
			// 
			this->startx->Location = System::Drawing::Point(104, 68);
			this->startx->Name = L"startx";
			this->startx->Size = System::Drawing::Size(100, 19);
			this->startx->TabIndex = 2;
			this->startx->Text = L"-49000";
			this->startx->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 71);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(85, 12);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Start X position";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 96);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(85, 12);
			this->label2->TabIndex = 4;
			this->label2->Text = L"Start Y position";
			// 
			// starty
			// 
			this->starty->Location = System::Drawing::Point(104, 93);
			this->starty->Name = L"starty";
			this->starty->Size = System::Drawing::Size(100, 19);
			this->starty->TabIndex = 5;
			this->starty->Text = L"-49000";
			this->starty->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(50, 124);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(39, 12);
			this->label3->TabIndex = 6;
			this->label3->Text = L"Step X";
			// 
			// stepx
			// 
			this->stepx->Location = System::Drawing::Point(104, 119);
			this->stepx->Name = L"stepx";
			this->stepx->Size = System::Drawing::Size(100, 19);
			this->stepx->TabIndex = 7;
			this->stepx->Text = L"10000";
			this->stepx->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(52, 151);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(39, 12);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Step Y";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(211, 71);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(20, 12);
			this->label5->TabIndex = 9;
			this->label5->Text = L"um";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(211, 96);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(20, 12);
			this->label6->TabIndex = 10;
			this->label6->Text = L"um";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(211, 124);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(20, 12);
			this->label7->TabIndex = 11;
			this->label7->Text = L"um";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(211, 151);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(20, 12);
			this->label8->TabIndex = 12;
			this->label8->Text = L"um";
			// 
			// stepy
			// 
			this->stepy->Location = System::Drawing::Point(104, 144);
			this->stepy->Name = L"stepy";
			this->stepy->Size = System::Drawing::Size(100, 19);
			this->stepy->TabIndex = 13;
			this->stepy->Text = L"10000";
			this->stepy->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(12, 173);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(79, 12);
			this->label9->TabIndex = 14;
			this->label9->Text = L"End X position";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(12, 199);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(79, 12);
			this->label10->TabIndex = 15;
			this->label10->Text = L"End Y position";
			// 
			// endx
			// 
			this->endx->Location = System::Drawing::Point(104, 170);
			this->endx->Name = L"endx";
			this->endx->Size = System::Drawing::Size(100, 19);
			this->endx->TabIndex = 16;
			this->endx->Text = L"49000";
			this->endx->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// endy
			// 
			this->endy->Location = System::Drawing::Point(104, 196);
			this->endy->Name = L"endy";
			this->endy->Size = System::Drawing::Size(100, 19);
			this->endy->TabIndex = 17;
			this->endy->Text = L"49000";
			this->endy->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(210, 173);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(20, 12);
			this->label11->TabIndex = 18;
			this->label11->Text = L"um";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(211, 199);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(20, 12);
			this->label12->TabIndex = 19;
			this->label12->Text = L"um";
			// 
			// ScanPositionViewer
			// 
			this->ScanPositionViewer->BackColor = System::Drawing::SystemColors::InfoText;
			this->ScanPositionViewer->Location = System::Drawing::Point(495, 290);
			this->ScanPositionViewer->Name = L"ScanPositionViewer";
			this->ScanPositionViewer->Size = System::Drawing::Size(467, 440);
			this->ScanPositionViewer->TabIndex = 20;
			this->ScanPositionViewer->TabStop = false;
			this->ScanPositionViewer->UseWaitCursor = true;
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(6) {
				L"Simple Test", L"Strip sensor photo", L"Strip sensor scan",
					L"Pixel sensor scan", L"Single Point", L"Line Scan"
			});
			this->comboBox1->Location = System::Drawing::Point(85, 21);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(119, 20);
			this->comboBox1->TabIndex = 2;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &ControlGUI::comboBox1_SelectedIndexChanged);
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->label13->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label13->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->label13->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 30, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label13->ForeColor = System::Drawing::Color::Red;
			this->label13->Location = System::Drawing::Point(304, 57);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(217, 42);
			this->label13->TabIndex = 23;
			this->label13->Text = L"Initializing...";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 30, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label14->Location = System::Drawing::Point(279, 12);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(146, 40);
			this->label14->TabIndex = 24;
			this->label14->Text = L"Status :";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(6, 24);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(69, 12);
			this->label15->TabIndex = 25;
			this->label15->Text = L"Scan Type : ";
			// 
			// timer1
			// 
			this->timer1->Interval = 50;
			this->timer1->Tick += gcnew System::EventHandler(this, &ControlGUI::timer1_Tick);
			// 
			// textTimer
			// 
			this->textTimer->Location = System::Drawing::Point(1082, 739);
			this->textTimer->Name = L"textTimer";
			this->textTimer->Size = System::Drawing::Size(47, 19);
			this->textTimer->TabIndex = 26;
			this->textTimer->Text = L"0";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(1131, 742);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(51, 12);
			this->label16->TabIndex = 27;
			this->label16->Text = L"s passed";
			// 
			// stopButton
			// 
			this->stopButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"stopButton.Image")));
			this->stopButton->Location = System::Drawing::Point(363, 15);
			this->stopButton->Name = L"stopButton";
			this->stopButton->Size = System::Drawing::Size(115, 111);
			this->stopButton->TabIndex = 29;
			this->stopButton->UseVisualStyleBackColor = true;
			this->stopButton->Click += gcnew System::EventHandler(this, &ControlGUI::stopButton_Click);
			// 
			// groupBox1
			// 
			this->groupBox1->BackColor = System::Drawing::SystemColors::MenuBar;
			this->groupBox1->Controls->Add(this->singlescan);
			this->groupBox1->Controls->Add(this->groupBox4);
			this->groupBox1->Controls->Add(this->groupBox2);
			this->groupBox1->Controls->Add(this->groupBox3);
			this->groupBox1->Location = System::Drawing::Point(968, 297);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(204, 433);
			this->groupBox1->TabIndex = 30;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Manual Operation Controler";
			// 
			// singlescan
			// 
			this->singlescan->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->singlescan->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->singlescan->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->singlescan->Location = System::Drawing::Point(13, 388);
			this->singlescan->Name = L"singlescan";
			this->singlescan->Size = System::Drawing::Size(178, 39);
			this->singlescan->TabIndex = 38;
			this->singlescan->Text = L"Single Scan";
			this->singlescan->UseVisualStyleBackColor = false;
			this->singlescan->Click += gcnew System::EventHandler(this, &ControlGUI::singlescan_Click);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->rbx10);
			this->groupBox4->Controls->Add(this->rbx5);
			this->groupBox4->Controls->Add(this->rbx2);
			this->groupBox4->Location = System::Drawing::Point(10, 161);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(182, 53);
			this->groupBox4->TabIndex = 4;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L" Selected Zoom Lens";
			// 
			// rbx10
			// 
			this->rbx10->AutoSize = true;
			this->rbx10->Location = System::Drawing::Point(116, 31);
			this->rbx10->Name = L"rbx10";
			this->rbx10->Size = System::Drawing::Size(41, 16);
			this->rbx10->TabIndex = 1;
			this->rbx10->Text = L"x10";
			this->rbx10->UseVisualStyleBackColor = true;
			this->rbx10->CheckedChanged += gcnew System::EventHandler(this, &ControlGUI::rbx10_CheckedChanged);
			// 
			// rbx5
			// 
			this->rbx5->AutoSize = true;
			this->rbx5->Location = System::Drawing::Point(60, 31);
			this->rbx5->Name = L"rbx5";
			this->rbx5->Size = System::Drawing::Size(35, 16);
			this->rbx5->TabIndex = 1;
			this->rbx5->Text = L"x5";
			this->rbx5->UseVisualStyleBackColor = true;
			this->rbx5->CheckedChanged += gcnew System::EventHandler(this, &ControlGUI::rbx5_CheckedChanged);
			// 
			// rbx2
			// 
			this->rbx2->AutoSize = true;
			this->rbx2->Checked = true;
			this->rbx2->Location = System::Drawing::Point(6, 30);
			this->rbx2->Name = L"rbx2";
			this->rbx2->Size = System::Drawing::Size(35, 16);
			this->rbx2->TabIndex = 0;
			this->rbx2->TabStop = true;
			this->rbx2->Text = L"x2";
			this->rbx2->UseVisualStyleBackColor = true;
			this->rbx2->CheckedChanged += gcnew System::EventHandler(this, &ControlGUI::rbx2_CheckedChanged);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->button4);
			this->groupBox2->Controls->Add(this->stageorig);
			this->groupBox2->Controls->Add(this->stageright);
			this->groupBox2->Controls->Add(this->TakeWafer);
			this->groupBox2->Controls->Add(this->stageleft);
			this->groupBox2->Controls->Add(this->stagedown);
			this->groupBox2->Controls->Add(this->stageup);
			this->groupBox2->Controls->Add(this->label19);
			this->groupBox2->Controls->Add(this->label18);
			this->groupBox2->Controls->Add(this->stepval);
			this->groupBox2->Location = System::Drawing::Point(6, 28);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(192, 127);
			this->groupBox2->TabIndex = 2;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"XY AXis Control";
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->button4->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->button4->Location = System::Drawing::Point(8, 56);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(46, 21);
			this->button4->TabIndex = 44;
			this->button4->Text = L"HOME";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &ControlGUI::button4_Click_1);
			// 
			// stageorig
			// 
			this->stageorig->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"stageorig.Image")));
			this->stageorig->Location = System::Drawing::Point(111, 51);
			this->stageorig->Name = L"stageorig";
			this->stageorig->Size = System::Drawing::Size(40, 40);
			this->stageorig->TabIndex = 7;
			this->stageorig->UseVisualStyleBackColor = true;
			this->stageorig->Click += gcnew System::EventHandler(this, &ControlGUI::stageorig_Click);
			// 
			// stageright
			// 
			this->stageright->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"stageright.Image")));
			this->stageright->Location = System::Drawing::Point(148, 51);
			this->stageright->Name = L"stageright";
			this->stageright->Size = System::Drawing::Size(35, 40);
			this->stageright->TabIndex = 6;
			this->stageright->UseVisualStyleBackColor = true;
			this->stageright->Click += gcnew System::EventHandler(this, &ControlGUI::stageright_Click);
			// 
			// TakeWafer
			// 
			this->TakeWafer->BackColor = System::Drawing::SystemColors::Info;
			this->TakeWafer->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->TakeWafer->ForeColor = System::Drawing::SystemColors::Highlight;
			this->TakeWafer->Location = System::Drawing::Point(-1, 83);
			this->TakeWafer->Name = L"TakeWafer";
			this->TakeWafer->Size = System::Drawing::Size(76, 44);
			this->TakeWafer->TabIndex = 43;
			this->TakeWafer->Text = L"Get wafer postion";
			this->TakeWafer->UseVisualStyleBackColor = false;
			this->TakeWafer->Click += gcnew System::EventHandler(this, &ControlGUI::button4_Click);
			// 
			// stageleft
			// 
			this->stageleft->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"stageleft.Image")));
			this->stageleft->Location = System::Drawing::Point(77, 51);
			this->stageleft->Name = L"stageleft";
			this->stageleft->Size = System::Drawing::Size(35, 40);
			this->stageleft->TabIndex = 5;
			this->stageleft->UseVisualStyleBackColor = true;
			this->stageleft->Click += gcnew System::EventHandler(this, &ControlGUI::stageleft_Click);
			// 
			// stagedown
			// 
			this->stagedown->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"stagedown.Image")));
			this->stagedown->Location = System::Drawing::Point(111, 88);
			this->stagedown->Name = L"stagedown";
			this->stagedown->Size = System::Drawing::Size(40, 35);
			this->stagedown->TabIndex = 4;
			this->stagedown->UseVisualStyleBackColor = true;
			this->stagedown->Click += gcnew System::EventHandler(this, &ControlGUI::stagedown_Click);
			// 
			// stageup
			// 
			this->stageup->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"stageup.Image")));
			this->stageup->Location = System::Drawing::Point(111, 18);
			this->stageup->Name = L"stageup";
			this->stageup->Size = System::Drawing::Size(40, 35);
			this->stageup->TabIndex = 3;
			this->stageup->UseVisualStyleBackColor = true;
			this->stageup->Click += gcnew System::EventHandler(this, &ControlGUI::stageup_Click);
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(52, 33);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(23, 12);
			this->label19->TabIndex = 2;
			this->label19->Text = L"mm";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(1, 15);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(37, 12);
			this->label18->TabIndex = 1;
			this->label18->Text = L"step : ";
			// 
			// stepval
			// 
			this->stepval->Location = System::Drawing::Point(3, 30);
			this->stepval->Name = L"stepval";
			this->stepval->Size = System::Drawing::Size(47, 19);
			this->stepval->TabIndex = 0;
			this->stepval->Text = L"10";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->movenear);
			this->groupBox3->Controls->Add(this->movefar);
			this->groupBox3->Controls->Add(this->button1);
			this->groupBox3->Controls->Add(this->AFposView);
			this->groupBox3->Location = System::Drawing::Point(10, 229);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(182, 139);
			this->groupBox3->TabIndex = 3;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Auto Focus Control";
			// 
			// movenear
			// 
			this->movenear->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"movenear.Image")));
			this->movenear->Location = System::Drawing::Point(137, 104);
			this->movenear->Name = L"movenear";
			this->movenear->Size = System::Drawing::Size(40, 35);
			this->movenear->TabIndex = 8;
			this->movenear->UseVisualStyleBackColor = true;
			this->movenear->Click += gcnew System::EventHandler(this, &ControlGUI::movenear_Click);
			// 
			// movefar
			// 
			this->movefar->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"movefar.Image")));
			this->movefar->Location = System::Drawing::Point(137, 47);
			this->movefar->Name = L"movefar";
			this->movefar->Size = System::Drawing::Size(40, 35);
			this->movefar->TabIndex = 8;
			this->movefar->UseVisualStyleBackColor = true;
			this->movefar->Click += gcnew System::EventHandler(this, &ControlGUI::movefar_Click);
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::SystemColors::HotTrack;
			this->button1->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button1->Location = System::Drawing::Point(136, 80);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(42, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"SC0";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &ControlGUI::button1_Click);
			// 
			// AFposView
			// 
			this->AFposView->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->AFposView->ForeColor = System::Drawing::SystemColors::MenuHighlight;
			this->AFposView->Location = System::Drawing::Point(6, 18);
			this->AFposView->Name = L"AFposView";
			this->AFposView->Size = System::Drawing::Size(170, 23);
			this->AFposView->TabIndex = 1;
			this->AFposView->Text = L"initialized...";
			this->AFposView->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->button2->ForeColor = System::Drawing::SystemColors::ButtonHighlight;
			this->button2->Location = System::Drawing::Point(1111, 3);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(63, 29);
			this->button2->TabIndex = 32;
			this->button2->Text = L"Quit";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &ControlGUI::button2_Click);
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->BackColor = System::Drawing::SystemColors::HighlightText;
			this->label17->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label17->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->label17->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label17->ForeColor = System::Drawing::Color::Blue;
			this->label17->Location = System::Drawing::Point(505, 296);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(114, 22);
			this->label17->TabIndex = 33;
			this->label17->Text = L"Initialized...";
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::SystemColors::MenuText;
			this->button3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"button3.Image")));
			this->button3->Location = System::Drawing::Point(892, 665);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(70, 64);
			this->button3->TabIndex = 34;
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &ControlGUI::button3_Click);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->label15);
			this->groupBox5->Controls->Add(this->startx);
			this->groupBox5->Controls->Add(this->label1);
			this->groupBox5->Controls->Add(this->label2);
			this->groupBox5->Controls->Add(this->starty);
			this->groupBox5->Controls->Add(this->label3);
			this->groupBox5->Controls->Add(this->stepx);
			this->groupBox5->Controls->Add(this->label4);
			this->groupBox5->Controls->Add(this->label5);
			this->groupBox5->Controls->Add(this->label6);
			this->groupBox5->Controls->Add(this->label7);
			this->groupBox5->Controls->Add(this->comboBox1);
			this->groupBox5->Controls->Add(this->label8);
			this->groupBox5->Controls->Add(this->stepy);
			this->groupBox5->Controls->Add(this->label9);
			this->groupBox5->Controls->Add(this->label12);
			this->groupBox5->Controls->Add(this->label10);
			this->groupBox5->Controls->Add(this->label11);
			this->groupBox5->Controls->Add(this->endx);
			this->groupBox5->Controls->Add(this->endy);
			this->groupBox5->Location = System::Drawing::Point(929, 54);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(243, 230);
			this->groupBox5->TabIndex = 35;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Scan Parameters";
			// 
			// picture
			// 
			this->picture->AutoSize = true;
			this->picture->ForeColor = System::Drawing::SystemColors::Highlight;
			this->picture->Location = System::Drawing::Point(863, 296);
			this->picture->Name = L"picture";
			this->picture->Size = System::Drawing::Size(89, 16);
			this->picture->TabIndex = 36;
			this->picture->Text = L"Take Picture";
			this->picture->UseVisualStyleBackColor = true;
			this->picture->CheckedChanged += gcnew System::EventHandler(this, &ControlGUI::picture_CheckedChanged);
			// 
			// afocus
			// 
			this->afocus->AutoSize = true;
			this->afocus->ForeColor = System::Drawing::SystemColors::Highlight;
			this->afocus->Location = System::Drawing::Point(863, 318);
			this->afocus->Name = L"afocus";
			this->afocus->Size = System::Drawing::Size(83, 16);
			this->afocus->TabIndex = 37;
			this->afocus->Text = L"Auto Focus";
			this->afocus->UseVisualStyleBackColor = true;
			this->afocus->CheckedChanged += gcnew System::EventHandler(this, &ControlGUI::afocus_CheckedChanged);
			// 
			// closeplot
			// 
			this->closeplot->AutoSize = true;
			this->closeplot->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->closeplot->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->closeplot->ForeColor = System::Drawing::SystemColors::ControlText;
			this->closeplot->Location = System::Drawing::Point(1086, 33);
			this->closeplot->Name = L"closeplot";
			this->closeplot->Size = System::Drawing::Size(86, 18);
			this->closeplot->TabIndex = 38;
			this->closeplot->Text = L"Close Plot";
			this->closeplot->UseVisualStyleBackColor = true;
			this->closeplot->CheckedChanged += gcnew System::EventHandler(this, &ControlGUI::closeplot_CheckedChanged);
			// 
			// serialnumber
			// 
			this->serialnumber->BackColor = System::Drawing::SystemColors::Info;
			this->serialnumber->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->serialnumber->Location = System::Drawing::Point(159, 27);
			this->serialnumber->Name = L"serialnumber";
			this->serialnumber->Size = System::Drawing::Size(171, 34);
			this->serialnumber->TabIndex = 39;
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label20->Location = System::Drawing::Point(18, 30);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(135, 27);
			this->label20->TabIndex = 40;
			this->label20->Text = L"Cassette #";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label21->Location = System::Drawing::Point(3, 216);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(136, 27);
			this->label21->TabIndex = 41;
			this->label21->Text = L"Processing";
			// 
			// textBox1
			// 
			this->textBox1->BackColor = System::Drawing::SystemColors::ControlLightLight;
			this->textBox1->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox1->Location = System::Drawing::Point(318, 250);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(39, 27);
			this->textBox1->TabIndex = 42;
			this->textBox1->Text = L"--";
			// 
			// groupBox6
			// 
			this->groupBox6->BackColor = System::Drawing::SystemColors::MenuBar;
			this->groupBox6->Controls->Add(this->comboBox2);
			this->groupBox6->Controls->Add(this->unload);
			this->groupBox6->Controls->Add(this->vacon);
			this->groupBox6->Controls->Add(this->vacoff);
			this->groupBox6->Controls->Add(this->combsetTray);
			this->groupBox6->Controls->Add(this->load);
			this->groupBox6->Controls->Add(this->WLcheck);
			this->groupBox6->Location = System::Drawing::Point(13, 426);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(476, 323);
			this->groupBox6->TabIndex = 44;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"Manual Operation for Wafer Loader";
			// 
			// comboBox2
			// 
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"Arm", L"Aligner", L"XYStage" });
			this->comboBox2->Location = System::Drawing::Point(25, 91);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(77, 20);
			this->comboBox2->TabIndex = 6;
			this->comboBox2->Text = L"XYStage";
			// 
			// unload
			// 
			this->unload->Location = System::Drawing::Point(111, 86);
			this->unload->Name = L"unload";
			this->unload->Size = System::Drawing::Size(60, 28);
			this->unload->TabIndex = 5;
			this->unload->Text = L"unload";
			this->unload->UseVisualStyleBackColor = true;
			this->unload->Click += gcnew System::EventHandler(this, &ControlGUI::unload_Click);
			// 
			// vacon
			// 
			this->vacon->Location = System::Drawing::Point(111, 120);
			this->vacon->Name = L"vacon";
			this->vacon->Size = System::Drawing::Size(60, 28);
			this->vacon->TabIndex = 5;
			this->vacon->Text = L"VAC on";
			this->vacon->UseVisualStyleBackColor = true;
			this->vacon->Click += gcnew System::EventHandler(this, &ControlGUI::vacon_Click);
			// 
			// vacoff
			// 
			this->vacoff->Location = System::Drawing::Point(50, 120);
			this->vacoff->Name = L"vacoff";
			this->vacoff->Size = System::Drawing::Size(60, 28);
			this->vacoff->TabIndex = 5;
			this->vacoff->Text = L"VAC off";
			this->vacoff->UseVisualStyleBackColor = true;
			this->vacoff->Click += gcnew System::EventHandler(this, &ControlGUI::vacoff_Click);
			// 
			// combsetTray
			// 
			this->combsetTray->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->combsetTray->FormattingEnabled = true;
			this->combsetTray->Items->AddRange(gcnew cli::array< System::Object^  >(25) {
				L"Tray#1", L"Tray#2", L"Tray#3", L"Tray#4", L"Tray#5",
					L"Tray#6", L"Tray#7", L"Tray#8", L"Tray#9", L"Tray#10", L"Tray#11", L"Tray#12", L"Tray#13", L"Tray#14", L"Tray#15", L"Tray#16",
					L"Tray#17", L"Tray#18", L"Tray#19", L"Tray#20", L"Tray#21", L"Tray#22", L"Tray#23", L"Tray#24", L"Tray#25"
			});
			this->combsetTray->Location = System::Drawing::Point(25, 49);
			this->combsetTray->Name = L"combsetTray";
			this->combsetTray->Size = System::Drawing::Size(80, 20);
			this->combsetTray->TabIndex = 4;
			this->combsetTray->Text = L"choose";
			this->combsetTray->SelectedIndexChanged += gcnew System::EventHandler(this, &ControlGUI::setTray_SelectedIndexChanged);
			// 
			// load
			// 
			this->load->Location = System::Drawing::Point(111, 44);
			this->load->Name = L"load";
			this->load->Size = System::Drawing::Size(53, 28);
			this->load->TabIndex = 3;
			this->load->Text = L"load";
			this->load->UseVisualStyleBackColor = true;
			this->load->Click += gcnew System::EventHandler(this, &ControlGUI::load_Click);
			// 
			// WLcheck
			// 
			this->WLcheck->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->WLcheck->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 13, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->WLcheck->Location = System::Drawing::Point(374, 19);
			this->WLcheck->Name = L"WLcheck";
			this->WLcheck->Size = System::Drawing::Size(95, 60);
			this->WLcheck->TabIndex = 0;
			this->WLcheck->Text = L"Check Status";
			this->WLcheck->UseVisualStyleBackColor = false;
			this->WLcheck->Click += gcnew System::EventHandler(this, &ControlGUI::WLcheck_Click);
			// 
			// WLmapping
			// 
			this->WLmapping->BackColor = System::Drawing::Color::DarkSeaGreen;
			this->WLmapping->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->WLmapping->ForeColor = System::Drawing::Color::Navy;
			this->WLmapping->Location = System::Drawing::Point(159, 72);
			this->WLmapping->Name = L"WLmapping";
			this->WLmapping->Size = System::Drawing::Size(103, 46);
			this->WLmapping->TabIndex = 2;
			this->WLmapping->Text = L"Mapping";
			this->WLmapping->UseVisualStyleBackColor = false;
			this->WLmapping->Click += gcnew System::EventHandler(this, &ControlGUI::WLmapping_Click);
			// 
			// WLinit
			// 
			this->WLinit->BackColor = System::Drawing::Color::MistyRose;
			this->WLinit->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->WLinit->ForeColor = System::Drawing::Color::Red;
			this->WLinit->Location = System::Drawing::Point(39, 72);
			this->WLinit->Name = L"WLinit";
			this->WLinit->Size = System::Drawing::Size(105, 46);
			this->WLinit->TabIndex = 1;
			this->WLinit->Text = L"Initialize";
			this->WLinit->UseVisualStyleBackColor = false;
			this->WLinit->Click += gcnew System::EventHandler(this, &ControlGUI::WLinit_Click);
			// 
			// groupBox7
			// 
			this->groupBox7->BackColor = System::Drawing::SystemColors::MenuHighlight;
			this->groupBox7->Controls->Add(this->label23);
			this->groupBox7->Controls->Add(this->textBox2);
			this->groupBox7->Controls->Add(this->label22);
			this->groupBox7->Controls->Add(this->stopButton);
			this->groupBox7->Controls->Add(this->startBotton);
			this->groupBox7->Controls->Add(this->serialnumber);
			this->groupBox7->Controls->Add(this->textBox1);
			this->groupBox7->Controls->Add(this->label20);
			this->groupBox7->Controls->Add(this->label21);
			this->groupBox7->Controls->Add(this->WLmapping);
			this->groupBox7->Controls->Add(this->WLinit);
			this->groupBox7->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->groupBox7->ForeColor = System::Drawing::SystemColors::HighlightText;
			this->groupBox7->Location = System::Drawing::Point(5, 135);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(484, 281);
			this->groupBox7->TabIndex = 45;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"Operation Window";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label23->Location = System::Drawing::Point(7, 253);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(81, 20);
			this->label23->TabIndex = 45;
			this->label23->Text = L"cassette";
			// 
			// textBox2
			// 
			this->textBox2->BackColor = System::Drawing::SystemColors::ControlLightLight;
			this->textBox2->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->textBox2->Location = System::Drawing::Point(94, 250);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(121, 27);
			this->textBox2->TabIndex = 44;
			this->textBox2->Text = L"--------";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->label22->Location = System::Drawing::Point(248, 253);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(64, 20);
			this->label22->TabIndex = 43;
			this->label22->Text = L"Tray #";
			// 
			// menuStrip1
			// 
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1184, 24);
			this->menuStrip1->TabIndex = 46;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// tray1
			// 
			this->tray1->AutoSize = true;
			this->tray1->BackColor = System::Drawing::Color::Gray;
			this->tray1->ForeColor = System::Drawing::SystemColors::Info;
			this->tray1->Location = System::Drawing::Point(65, 48);
			this->tray1->Name = L"tray1";
			this->tray1->Size = System::Drawing::Size(58, 12);
			this->tray1->TabIndex = 47;
			this->tray1->Text = L"   Tray1   ";
			// 
			// tray2
			// 
			this->tray2->AutoSize = true;
			this->tray2->BackColor = System::Drawing::Color::Gray;
			this->tray2->ForeColor = System::Drawing::SystemColors::Info;
			this->tray2->Location = System::Drawing::Point(65, 63);
			this->tray2->Name = L"tray2";
			this->tray2->Size = System::Drawing::Size(58, 12);
			this->tray2->TabIndex = 48;
			this->tray2->Text = L"   Tray2   ";
			// 
			// tray4
			// 
			this->tray4->AutoSize = true;
			this->tray4->BackColor = System::Drawing::Color::Gray;
			this->tray4->ForeColor = System::Drawing::SystemColors::Info;
			this->tray4->Location = System::Drawing::Point(65, 93);
			this->tray4->Name = L"tray4";
			this->tray4->Size = System::Drawing::Size(58, 12);
			this->tray4->TabIndex = 50;
			this->tray4->Text = L"   Tray4   ";
			// 
			// tray3
			// 
			this->tray3->AutoSize = true;
			this->tray3->BackColor = System::Drawing::Color::Gray;
			this->tray3->ForeColor = System::Drawing::SystemColors::Info;
			this->tray3->Location = System::Drawing::Point(65, 78);
			this->tray3->Name = L"tray3";
			this->tray3->Size = System::Drawing::Size(58, 12);
			this->tray3->TabIndex = 49;
			this->tray3->Text = L"   Tray3   ";
			// 
			// tray8
			// 
			this->tray8->AutoSize = true;
			this->tray8->BackColor = System::Drawing::Color::Gray;
			this->tray8->ForeColor = System::Drawing::SystemColors::Info;
			this->tray8->Location = System::Drawing::Point(65, 153);
			this->tray8->Name = L"tray8";
			this->tray8->Size = System::Drawing::Size(58, 12);
			this->tray8->TabIndex = 54;
			this->tray8->Text = L"   Tray8   ";
			// 
			// tray7
			// 
			this->tray7->AutoSize = true;
			this->tray7->BackColor = System::Drawing::Color::Gray;
			this->tray7->ForeColor = System::Drawing::SystemColors::Info;
			this->tray7->Location = System::Drawing::Point(65, 138);
			this->tray7->Name = L"tray7";
			this->tray7->Size = System::Drawing::Size(58, 12);
			this->tray7->TabIndex = 53;
			this->tray7->Text = L"   Tray7   ";
			// 
			// tray6
			// 
			this->tray6->AutoSize = true;
			this->tray6->BackColor = System::Drawing::Color::Gray;
			this->tray6->ForeColor = System::Drawing::SystemColors::Info;
			this->tray6->Location = System::Drawing::Point(65, 123);
			this->tray6->Name = L"tray6";
			this->tray6->Size = System::Drawing::Size(58, 12);
			this->tray6->TabIndex = 52;
			this->tray6->Text = L"   Tray6   ";
			// 
			// tray5
			// 
			this->tray5->AutoSize = true;
			this->tray5->BackColor = System::Drawing::Color::Gray;
			this->tray5->ForeColor = System::Drawing::SystemColors::Info;
			this->tray5->Location = System::Drawing::Point(65, 108);
			this->tray5->Name = L"tray5";
			this->tray5->Size = System::Drawing::Size(58, 12);
			this->tray5->TabIndex = 51;
			this->tray5->Text = L"   Tray5   ";
			// 
			// tray13
			// 
			this->tray13->AutoSize = true;
			this->tray13->BackColor = System::Drawing::Color::Gray;
			this->tray13->ForeColor = System::Drawing::SystemColors::Info;
			this->tray13->Location = System::Drawing::Point(65, 228);
			this->tray13->Name = L"tray13";
			this->tray13->Size = System::Drawing::Size(60, 12);
			this->tray13->TabIndex = 59;
			this->tray13->Text = L"   Tray13  ";
			// 
			// tray12
			// 
			this->tray12->AutoSize = true;
			this->tray12->BackColor = System::Drawing::Color::Gray;
			this->tray12->ForeColor = System::Drawing::SystemColors::Info;
			this->tray12->Location = System::Drawing::Point(65, 213);
			this->tray12->Name = L"tray12";
			this->tray12->Size = System::Drawing::Size(60, 12);
			this->tray12->TabIndex = 58;
			this->tray12->Text = L"   Tray12  ";
			// 
			// tray11
			// 
			this->tray11->AutoSize = true;
			this->tray11->BackColor = System::Drawing::Color::Gray;
			this->tray11->ForeColor = System::Drawing::SystemColors::Info;
			this->tray11->Location = System::Drawing::Point(65, 198);
			this->tray11->Name = L"tray11";
			this->tray11->Size = System::Drawing::Size(60, 12);
			this->tray11->TabIndex = 57;
			this->tray11->Text = L"   Tray11  ";
			// 
			// tray10
			// 
			this->tray10->AutoSize = true;
			this->tray10->BackColor = System::Drawing::Color::Gray;
			this->tray10->ForeColor = System::Drawing::SystemColors::Info;
			this->tray10->Location = System::Drawing::Point(65, 183);
			this->tray10->Name = L"tray10";
			this->tray10->Size = System::Drawing::Size(60, 12);
			this->tray10->TabIndex = 56;
			this->tray10->Text = L"   Tray10  ";
			// 
			// tray9
			// 
			this->tray9->AutoSize = true;
			this->tray9->BackColor = System::Drawing::Color::Gray;
			this->tray9->ForeColor = System::Drawing::SystemColors::Info;
			this->tray9->Location = System::Drawing::Point(65, 168);
			this->tray9->Name = L"tray9";
			this->tray9->Size = System::Drawing::Size(58, 12);
			this->tray9->TabIndex = 55;
			this->tray9->Text = L"   Tray9   ";
			// 
			// tray25
			// 
			this->tray25->AutoSize = true;
			this->tray25->BackColor = System::Drawing::Color::Gray;
			this->tray25->ForeColor = System::Drawing::SystemColors::Info;
			this->tray25->Location = System::Drawing::Point(141, 213);
			this->tray25->Name = L"tray25";
			this->tray25->Size = System::Drawing::Size(60, 12);
			this->tray25->TabIndex = 71;
			this->tray25->Text = L"   Tray25  ";
			// 
			// tray24
			// 
			this->tray24->AutoSize = true;
			this->tray24->BackColor = System::Drawing::Color::Gray;
			this->tray24->ForeColor = System::Drawing::SystemColors::Info;
			this->tray24->Location = System::Drawing::Point(141, 198);
			this->tray24->Name = L"tray24";
			this->tray24->Size = System::Drawing::Size(60, 12);
			this->tray24->TabIndex = 70;
			this->tray24->Text = L"   Tray24  ";
			// 
			// tray23
			// 
			this->tray23->AutoSize = true;
			this->tray23->BackColor = System::Drawing::Color::Gray;
			this->tray23->ForeColor = System::Drawing::SystemColors::Info;
			this->tray23->Location = System::Drawing::Point(141, 183);
			this->tray23->Name = L"tray23";
			this->tray23->Size = System::Drawing::Size(60, 12);
			this->tray23->TabIndex = 69;
			this->tray23->Text = L"   Tray23  ";
			// 
			// tray22
			// 
			this->tray22->AutoSize = true;
			this->tray22->BackColor = System::Drawing::Color::Gray;
			this->tray22->ForeColor = System::Drawing::SystemColors::Info;
			this->tray22->Location = System::Drawing::Point(141, 168);
			this->tray22->Name = L"tray22";
			this->tray22->Size = System::Drawing::Size(60, 12);
			this->tray22->TabIndex = 68;
			this->tray22->Text = L"   Tray22  ";
			// 
			// tray21
			// 
			this->tray21->AutoSize = true;
			this->tray21->BackColor = System::Drawing::Color::Gray;
			this->tray21->ForeColor = System::Drawing::SystemColors::Info;
			this->tray21->Location = System::Drawing::Point(141, 153);
			this->tray21->Name = L"tray21";
			this->tray21->Size = System::Drawing::Size(60, 12);
			this->tray21->TabIndex = 67;
			this->tray21->Text = L"   Tray21  ";
			// 
			// tray20
			// 
			this->tray20->AutoSize = true;
			this->tray20->BackColor = System::Drawing::Color::Gray;
			this->tray20->ForeColor = System::Drawing::SystemColors::Info;
			this->tray20->Location = System::Drawing::Point(141, 138);
			this->tray20->Name = L"tray20";
			this->tray20->Size = System::Drawing::Size(60, 12);
			this->tray20->TabIndex = 66;
			this->tray20->Text = L"   Tray20  ";
			// 
			// tray19
			// 
			this->tray19->AutoSize = true;
			this->tray19->BackColor = System::Drawing::Color::Gray;
			this->tray19->ForeColor = System::Drawing::SystemColors::Info;
			this->tray19->Location = System::Drawing::Point(141, 123);
			this->tray19->Name = L"tray19";
			this->tray19->Size = System::Drawing::Size(60, 12);
			this->tray19->TabIndex = 65;
			this->tray19->Text = L"   Tray19  ";
			// 
			// tray18
			// 
			this->tray18->AutoSize = true;
			this->tray18->BackColor = System::Drawing::Color::Gray;
			this->tray18->ForeColor = System::Drawing::SystemColors::Info;
			this->tray18->Location = System::Drawing::Point(141, 108);
			this->tray18->Name = L"tray18";
			this->tray18->Size = System::Drawing::Size(60, 12);
			this->tray18->TabIndex = 64;
			this->tray18->Text = L"   Tray18  ";
			// 
			// tray17
			// 
			this->tray17->AutoSize = true;
			this->tray17->BackColor = System::Drawing::Color::Gray;
			this->tray17->ForeColor = System::Drawing::SystemColors::Info;
			this->tray17->Location = System::Drawing::Point(141, 93);
			this->tray17->Name = L"tray17";
			this->tray17->Size = System::Drawing::Size(60, 12);
			this->tray17->TabIndex = 63;
			this->tray17->Text = L"   Tray17  ";
			// 
			// tray16
			// 
			this->tray16->AutoSize = true;
			this->tray16->BackColor = System::Drawing::Color::Gray;
			this->tray16->ForeColor = System::Drawing::SystemColors::Info;
			this->tray16->Location = System::Drawing::Point(141, 78);
			this->tray16->Name = L"tray16";
			this->tray16->Size = System::Drawing::Size(60, 12);
			this->tray16->TabIndex = 62;
			this->tray16->Text = L"   Tray16  ";
			// 
			// tray15
			// 
			this->tray15->AutoSize = true;
			this->tray15->BackColor = System::Drawing::Color::Gray;
			this->tray15->ForeColor = System::Drawing::SystemColors::Info;
			this->tray15->Location = System::Drawing::Point(141, 63);
			this->tray15->Name = L"tray15";
			this->tray15->Size = System::Drawing::Size(60, 12);
			this->tray15->TabIndex = 61;
			this->tray15->Text = L"   Tray15  ";
			// 
			// tray14
			// 
			this->tray14->AutoSize = true;
			this->tray14->BackColor = System::Drawing::Color::Gray;
			this->tray14->ForeColor = System::Drawing::SystemColors::Info;
			this->tray14->Location = System::Drawing::Point(141, 48);
			this->tray14->Name = L"tray14";
			this->tray14->Size = System::Drawing::Size(60, 12);
			this->tray14->TabIndex = 60;
			this->tray14->Text = L"   Tray14  ";
			// 
			// cassette
			// 
			this->cassette->AutoSize = true;
			this->cassette->BackColor = System::Drawing::Color::Gray;
			this->cassette->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(128)));
			this->cassette->ForeColor = System::Drawing::SystemColors::Info;
			this->cassette->Location = System::Drawing::Point(79, 15);
			this->cassette->Name = L"cassette";
			this->cassette->Size = System::Drawing::Size(113, 27);
			this->cassette->TabIndex = 72;
			this->cassette->Text = L"Cassette";
			// 
			// groupBox8
			// 
			this->groupBox8->Controls->Add(this->OrigInit);
			this->groupBox8->Controls->Add(this->pictureBox4);
			this->groupBox8->Controls->Add(this->pictureBox3);
			this->groupBox8->Controls->Add(this->pictureBox2);
			this->groupBox8->Controls->Add(this->cassette);
			this->groupBox8->Controls->Add(this->tray25);
			this->groupBox8->Controls->Add(this->tray24);
			this->groupBox8->Controls->Add(this->tray23);
			this->groupBox8->Controls->Add(this->tray22);
			this->groupBox8->Controls->Add(this->tray21);
			this->groupBox8->Controls->Add(this->tray20);
			this->groupBox8->Controls->Add(this->tray19);
			this->groupBox8->Controls->Add(this->tray18);
			this->groupBox8->Controls->Add(this->tray17);
			this->groupBox8->Controls->Add(this->tray16);
			this->groupBox8->Controls->Add(this->tray15);
			this->groupBox8->Controls->Add(this->tray14);
			this->groupBox8->Controls->Add(this->tray13);
			this->groupBox8->Controls->Add(this->tray12);
			this->groupBox8->Controls->Add(this->tray11);
			this->groupBox8->Controls->Add(this->tray10);
			this->groupBox8->Controls->Add(this->tray9);
			this->groupBox8->Controls->Add(this->tray8);
			this->groupBox8->Controls->Add(this->tray7);
			this->groupBox8->Controls->Add(this->tray6);
			this->groupBox8->Controls->Add(this->tray5);
			this->groupBox8->Controls->Add(this->tray4);
			this->groupBox8->Controls->Add(this->tray3);
			this->groupBox8->Controls->Add(this->tray2);
			this->groupBox8->Controls->Add(this->tray1);
			this->groupBox8->ForeColor = System::Drawing::Color::Blue;
			this->groupBox8->Location = System::Drawing::Point(709, 33);
			this->groupBox8->Name = L"groupBox8";
			this->groupBox8->Size = System::Drawing::Size(214, 251);
			this->groupBox8->TabIndex = 73;
			this->groupBox8->TabStop = false;
			this->groupBox8->Text = L"status";
			// 
			// pictureBox4
			// 
			this->pictureBox4->BackColor = System::Drawing::SystemColors::HotTrack;
			this->pictureBox4->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox4.Image")));
			this->pictureBox4->InitialImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox4.InitialImage")));
			this->pictureBox4->Location = System::Drawing::Point(10, 175);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(45, 38);
			this->pictureBox4->TabIndex = 75;
			this->pictureBox4->TabStop = false;
			// 
			// pictureBox3
			// 
			this->pictureBox3->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.Image")));
			this->pictureBox3->InitialImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.InitialImage")));
			this->pictureBox3->Location = System::Drawing::Point(10, 115);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(46, 38);
			this->pictureBox3->TabIndex = 74;
			this->pictureBox3->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->InitialImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.InitialImage")));
			this->pictureBox2->Location = System::Drawing::Point(10, 56);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(47, 39);
			this->pictureBox2->TabIndex = 73;
			this->pictureBox2->TabStop = false;
			// 
			// picjusttaken
			// 
			this->picjusttaken->Location = System::Drawing::Point(495, 135);
			this->picjusttaken->Name = L"picjusttaken";
			this->picjusttaken->Size = System::Drawing::Size(210, 140);
			this->picjusttaken->TabIndex = 74;
			this->picjusttaken->TabStop = false;
			// 
			// OrigInit
			// 
			this->OrigInit->AutoSize = true;
			this->OrigInit->BackColor = System::Drawing::Color::Gray;
			this->OrigInit->Font = (gcnew System::Drawing::Font(L"MS UI Gothic", 15));
			this->OrigInit->ForeColor = System::Drawing::SystemColors::Info;
			this->OrigInit->Location = System::Drawing::Point(6, 21);
			this->OrigInit->Name = L"OrigInit";
			this->OrigInit->Size = System::Drawing::Size(44, 20);
			this->OrigInit->TabIndex = 76;
			this->OrigInit->Text = L"INIT";
			// 
			// ControlGUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::Window;
			this->ClientSize = System::Drawing::Size(1184, 761);
			this->Controls->Add(this->picjusttaken);
			this->Controls->Add(this->groupBox8);
			this->Controls->Add(this->groupBox7);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->closeplot);
			this->Controls->Add(this->afocus);
			this->Controls->Add(this->picture);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->label17);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->label16);
			this->Controls->Add(this->textTimer);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->ScanPositionViewer);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"ControlGUI";
			this->Text = L"ControlGUI";
			this->Load += gcnew System::EventHandler(this, &ControlGUI::ControlGUI_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ScanPositionViewer))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			this->groupBox8->ResumeLayout(false);
			this->groupBox8->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picjusttaken))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		void PatternMatching() {
			std::cout<< "check return value "<< ms0.getRs232cAFCtrl()->CheckZoom()<<std::endl;
			// memory previous zoom value
			int previouszoom = ms0.GetZoom();
			ms0.SetZoom(2);
			ms0.PatternMatchingScan();
			// put back zoom value
			ms0.SetZoom(previouszoom);
		}
		void FullTrayAutoScan() {
			// will implement mapping checker here
			if (0)return;

			for (int itray = 0; itray < 25; itray++) {
				traynum = itray + 1;
				if (ms0.getRs232cWLCtrl()->GetTS(itray) != 1) {
					std::cout << "tray status of tray:" << traynum << ms0.getRs232cWLCtrl()->GetTS(itray) << std::endl;
					continue;
				}
				std::cout << "processing tray " << traynum << std::endl;

//				OneTrayLoad();
				MoveTakeWaferPosition();
				int ret0 = ms0.LoadOneTray(traynum);
//				OneTrayLoad();
				Scanning();
				MoveTakeWaferPosition();
				int ret1 = ms0.UnLoadOneTray(traynum);

//				OneTrayUnload();
			}
		}
		void Scanning() { // all process need to be done here.
	
			ms0.getRs232cXYCtrl()->Absolute2D(SCANORIGPOSX, SCANORIGPOSY);
			ms0.getRs232cXYCtrl()->PrintPosition();
			DrawScanPosition();

			// change zoom to x2 to take picture
			ms0.SetZoom(2);

//			PatternMatching();

			ms0.SetScanMode(1);
			//comboBox1->SelectedIndex = 2;

			// scan
			//ScanningSingle(-1* STRIPW/2, -1* STRIPH/2, PHOTOW*PIXELSIZE/2, PHOTOH*PIXELSIZE/2, STRIPW/2, STRIPH/2);
			ScanningSingle(-1 * STRIPW / 2, -1 * STRIPH / 2,  30000,  30000 , STRIPW / 2, STRIPH / 2);

			// change zoom to x10 for height measurement
			ms0.SetZoom(10);
			ms0.SetScanMode(0);
			ms0.VacuumControl(0);
			ms0.getRs232cWLCtrl()->CheckStatus(false, false);
			Sleep(200);

			//ScanningSingle(-1 * STRIPW / 2, -1 * STRIPH / 2, 10000, 10000, STRIPW / 2, STRIPH / 2);
			ScanningSingle(-1 * STRIPW / 2, -1 * STRIPH / 2, 30000, 30000, STRIPW / 2, STRIPH / 2);
			ms0.VacuumControl(1);
		}
		void ScanningSingle(float startx, float starty, float stepx, float stepy, float endx, float endy) {
			//			ms0.ROOTtest();
			
			scanRunning = true;
			int ret = ms0.RunScan(startx, starty, stepx, stepy, endx, endy);
			if (ret != 0)MessageBox::Show("RunScanPhoto() failed... ");

			scanRunning = false;
			scanfinished = true;
		}
		void MoveTakeWaferPosition() {
			ms0.getRs232cAFCtrl()->ClearAF();
			ms0.getRs232cXYCtrl()->Absolute2D(TAKEWAFERPOSX, TAKEWAFERPOSY);
		//				Sleep(5000);
		//				ms0.getRs232cXYCtrl()->Absolute2D(0,0);
						ms0.SetWaferLoadPosition();
		}
		void MoveScanningPosition() {
			ms0.getRs232cXYCtrl()->Absolute2D(0,0);
			ms0.SetScanningPosition();

		}

		void ShowWaferLoaderErrors() {
			System::String^ mess_str = gcnew System::String((ms0.getRs232cWLCtrl()->GetError()).c_str());
			System::Windows::Forms::MessageBox::Show(mess_str);
		}
		void ScanningSingle() {
			float _startx = float::Parse(startx->Text); // [um]
			float _starty = float::Parse(starty->Text); // [um]
			float _stepx = float::Parse(stepx->Text); // [um]
			float _stepy = float::Parse(stepy->Text); // [um]
			float _endx = float::Parse(endx->Text); // [um]     negative ment until the max value
			float _endy = float::Parse(endy->Text); // [um]     negative ment until the max value
			ScanningSingle(_startx, _starty, _stepx, _stepy, _endx, _endy);
		}
		void AutoFocus() {
			if (AFlock)return;
			ms0.getRs232cAFCtrl()->SendSC0(1);
			Sleep(200);
			ms0.getRs232cAFCtrl()->CheckStatus();
			Sleep(200);
			ms0.getRs232cAFCtrl()->GetZPos();
			ms0.getRs232cAFCtrl()->ResumeAF();
		}
		void MoveAFZpos(char dir, int pulse) {
			ms0.getRs232cAFCtrl()->MoveZpos(dir,pulse);
			Sleep(200);
			ms0.getRs232cAFCtrl()->CheckStatus();
			Sleep(200);
			ms0.getRs232cAFCtrl()->GetZPos();

		}

		void ShowZoom() {
//			std::cout << "(" << ms0.getRs232cAFCtrl()->m_AFZoom << ")" << std::endl;
			String^ str;
			str = gcnew String(ms0.getRs232cAFCtrl()->m_AFZoom);
			if (str == L"X05") {
				rbx2->Checked = true;
			}
			else if (str == L"X10") {
				rbx5->Checked = true;
			}
			else if (str == L"X20") {
				rbx10->Checked = true;
			}
			else {
				std::cout << "strange zoom lens : " << ms0.getRs232cAFCtrl()->m_AFZoom << std::endl;
			}
		}
		void ShowLoaderStatus() {
			if (ms0.getRs232cWLCtrl()->GetVACS() == 0) {
				this->vacon->BackColor = System::Drawing::Color::Gray;
				this->vacoff->BackColor = System::Drawing::Color::Lime;
			}
			else if (ms0.getRs232cWLCtrl()->GetVACS() == 1) {
				this->vacon->BackColor = System::Drawing::Color::Lime;
				this->vacoff->BackColor = System::Drawing::Color::Gray;

			}
			if (ms0.getRs232cWLCtrl()->GetORGS() == 0)this->OrigInit->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetORGS() == 1)this->OrigInit->BackColor = System::Drawing::Color::Lime;
			if (ms0.getRs232cWLCtrl()->GetCS() == 0)this->cassette->BackColor = System::Drawing::Color::Red;
			else if (ms0.getRs232cWLCtrl()->GetCS() == 1)this->cassette->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetCS() == 2)this->cassette->BackColor = System::Drawing::Color::Gray;
			if (ms0.getRs232cWLCtrl()->GetTS(0) == 0)this->tray1->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(0) == 1)this->tray1->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(0) == 2)this->tray1->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(0) == 3)this->tray1->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(0) == 4)this->tray1->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(1) == 0)this->tray2->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(1) == 1)this->tray2->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(1) == 2)this->tray2->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(1) == 3)this->tray2->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(1) == 4)this->tray2->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(2) == 0)this->tray3->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(2) == 1)this->tray3->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(2) == 2)this->tray3->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(2) == 3)this->tray3->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(2) == 4)this->tray3->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(3) == 0)this->tray4->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(3) == 1)this->tray4->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(3) == 2)this->tray4->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(3) == 3)this->tray4->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(3) == 4)this->tray4->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(4) == 0)this->tray5->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(4) == 1)this->tray5->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(4) == 2)this->tray5->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(4) == 3)this->tray5->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(4) == 4)this->tray5->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(5) == 0)this->tray6->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(5) == 1)this->tray6->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(5) == 2)this->tray6->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(5) == 3)this->tray6->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(5) == 4)this->tray6->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(6) == 0)this->tray7->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(6) == 1)this->tray7->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(6) == 2)this->tray7->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(6) == 3)this->tray7->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(6) == 4)this->tray7->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(7) == 0)this->tray8->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(7) == 1)this->tray8->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(7) == 2)this->tray8->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(7) == 3)this->tray8->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(7) == 4)this->tray8->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(8) == 0)this->tray9->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(8) == 1)this->tray9->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(8) == 2)this->tray9->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(8) == 3)this->tray9->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(8) == 4)this->tray9->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(9) == 0)this->tray10->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(9) == 1)this->tray10->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(9) == 2)this->tray10->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(9) == 3)this->tray10->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(9) == 4)this->tray10->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(10) == 0)this->tray11->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(10) == 1)this->tray11->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(10) == 2)this->tray11->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(10) == 3)this->tray11->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(10) == 4)this->tray11->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(11) == 0)this->tray12->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(11) == 1)this->tray12->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(11) == 2)this->tray12->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(11) == 3)this->tray12->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(11) == 4)this->tray12->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(12) == 0)this->tray13->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(12) == 1)this->tray13->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(12) == 2)this->tray13->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(12) == 3)this->tray13->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(12) == 4)this->tray13->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(13) == 0)this->tray14->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(13) == 1)this->tray14->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(13) == 2)this->tray14->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(13) == 3)this->tray14->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(13) == 4)this->tray14->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(14) == 0)this->tray15->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(14) == 1)this->tray15->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(14) == 2)this->tray15->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(14) == 3)this->tray15->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(14) == 4)this->tray15->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(15) == 0)this->tray16->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(15) == 1)this->tray16->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(15) == 2)this->tray16->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(15) == 3)this->tray16->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(15) == 4)this->tray16->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(16) == 0)this->tray17->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(16) == 1)this->tray17->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(16) == 2)this->tray17->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(16) == 3)this->tray17->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(16) == 4)this->tray17->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(17) == 0)this->tray18->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(17) == 1)this->tray18->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(17) == 2)this->tray18->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(17) == 3)this->tray18->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(17) == 4)this->tray18->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(18) == 0)this->tray19->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(18) == 1)this->tray19->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(18) == 2)this->tray19->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(18) == 3)this->tray19->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(18) == 4)this->tray19->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(19) == 0)this->tray20->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(19) == 1)this->tray20->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(19) == 2)this->tray20->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(19) == 3)this->tray20->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(19) == 4)this->tray20->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(20) == 0)this->tray21->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(20) == 1)this->tray21->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(20) == 2)this->tray21->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(20) == 3)this->tray21->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(20) == 4)this->tray21->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(21) == 0)this->tray22->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(21) == 1)this->tray22->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(21) == 2)this->tray22->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(21) == 3)this->tray22->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(21) == 4)this->tray22->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(22) == 0)this->tray23->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(22) == 1)this->tray23->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(22) == 2)this->tray23->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(22) == 3)this->tray23->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(22) == 4)this->tray23->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(23) == 0)this->tray24->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(23) == 1)this->tray24->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(23) == 2)this->tray24->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(23) == 3)this->tray24->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(23) == 4)this->tray24->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->GetTS(24) == 0)this->tray25->BackColor = System::Drawing::Color::Gray;
			else if (ms0.getRs232cWLCtrl()->GetTS(24) == 1)this->tray25->BackColor = System::Drawing::Color::Lime;
			else if (ms0.getRs232cWLCtrl()->GetTS(24) == 2)this->tray25->BackColor = System::Drawing::Color::Magenta;
			else if (ms0.getRs232cWLCtrl()->GetTS(24) == 3)this->tray25->BackColor = System::Drawing::Color::Blue;
			else if (ms0.getRs232cWLCtrl()->GetTS(24) == 4)this->tray25->BackColor = System::Drawing::Color::Red;
			if (ms0.getRs232cWLCtrl()->Getarm() == 0) this->pictureBox2->Image = Image::FromFile("Arm.png");
			else if(ms0.getRs232cWLCtrl()->Getarm() == 1)this->pictureBox2->Image = Image::FromFile("ArmY.png");
			if (ms0.getRs232cWLCtrl()->Getaligner() == 0) this->pictureBox3->Image = Image::FromFile("Align.png");
			else if (ms0.getRs232cWLCtrl()->Getaligner() == 1)this->pictureBox3->Image = Image::FromFile("AlignY.png");
			if (ms0.getRs232cWLCtrl()->GetXYstage() == 0) this->pictureBox4->Image = Image::FromFile("stage.png");
			else if (ms0.getRs232cWLCtrl()->GetXYstage() == 1)this->pictureBox4->Image = Image::FromFile("stageY.png");

		}

		void ShowPictureJustTaken() {
//			std::string filename=ms0.getDataHandler()->GetLastPictureName();
			System::String^ picname = gcnew String((ms0.getDataHandler()->GetLastPictureName()).c_str());
			if ( picname != L"SAME"  && lastpicname !=picname ) {
				std::cout << "drawing picture" << std::endl;

				img=Image::FromFile(picname);
				g->DrawImage(img,0,0,img->Width*0.035, img->Height*0.035);
				this->picjusttaken->Image = canvas;
			}
			lastpicname = picname;
		}
		void DrawScanPosition() {
			float xpos = ms0.getRs232cXYCtrl()->m_fAValue;
			float ypos = ms0.getRs232cXYCtrl()->m_fBValue;
			scanmark->ChagePreviousCircle();
			scanmark->PutCircle(xpos, ypos);
		}

#pragma endregion
	private: System::Void ControlGUI_Load(System::Object^  sender, System::EventArgs^  e) {
		canvas = gcnew Bitmap(this->picjusttaken->Width, this->picjusttaken->Height);
		g = Graphics::FromImage(canvas);
		System::String^ samplepic= gcnew String((ms0.getDataHandler()->GetSamplePictureName()).c_str());
	//	img = Image::FromFile(samplepic);
	//	g->DrawImage(img, 0, 0, img->Width * 0.035, img->Height * 0.035);
	//	this->picjusttaken->Image = canvas;
		nowTime = 0; 
		emergencystop = false;
		timer1->Start();
		int height = ScanPositionViewer->Height;
		int width = ScanPositionViewer->Width;
		ScanPositionCanvas = gcnew Bitmap(width, height);
		ms0.SetCOMPorts(COMXY, COMAF, COMRV, COMWL);
		ms0.InitializeScan((float)height, (float)width, SCANORIGPOSX, SCANORIGPOSY);
		std::cout << " Initialize all system done." << std::endl;
		std::cout << "zoom lens :" << std::endl;
		ShowZoom();
		scanmark = gcnew MoveScanMarker::ScanMarker(ScanPositionViewer, ScanPositionCanvas, Brushes::LightBlue,ms0.cs0); 
		label13->Text = L"Ready";
		label13->ForeColor = System::Drawing::Color::Green;
		scanmark->SetSensorType("strip");
		DrawScanPosition();
		AFlock = false; // AF unlock
		std::cout << "Control GUI successfully loaded" << std::endl;
	}



	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {

		//////////////////////////////////////////////////////////////////////////////
		//		std::cout << "drawing scan position" << std::endl;
		if(scanRunning)DrawScanPosition();
		if (startBotton->Enabled == false && scanfinished == true) {
			startBotton->Enabled = true;
			scanfinished = false;
			if (emergencystop == false) {
				label13->Text = L"Scan finished";
				label13->ForeColor = System::Drawing::Color::Green;
			}
			else {
				label13->Text = L"Emergency Stop";
				label13->ForeColor = System::Drawing::Color::Red;
			}
		}
		//////////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////////
		//		std::cout << "getting AF status and zpos" << std::endl;
		char st[256];
		if (strlen(st) != 0 || st != "") {
			String^ str;
			str = gcnew String(ms0.getRs232cAFCtrl()->m_AFStatus);

			if (str == L"Error") {
				this->label17->ForeColor = System::Drawing::Color::Red;

			}
			else {
				this->label17->ForeColor = System::Drawing::Color::Green;
			}
			label17->Text = str;
		}
		else {
			std::cout << "strange length " << std::endl;
		}
		char czpos[256];
		sprintf_s(czpos, sizeof(czpos), "%5.1f um  (%d)", ms0.getRs232cAFCtrl()->m_AFZposum, ms0.getRs232cAFCtrl()->m_AFZpos);
		String^ str;
		str = gcnew String(czpos);
		AFposView->Text = str;
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//		std::cout << "getting ScanMode" << std::endl;
		int type=ms0.GetScanmode();
		if (type & 0b1)this->picture->Checked = true;
		if (type & 0b10)this->afocus->Checked = true;
		if (ms0.GetClosePlot())closeplot->Checked = true;


		//////////////////////////////////////////////////////////////////////////////
		if (WLinit->Enabled == false && wlinitfinished == true) {
			WLinit->Enabled = true;
			wlinitfinished = false;
			label13->Text = L"Loader Init finished";
			label13->ForeColor = System::Drawing::Color::Green;
		}
		//////////////////////////////////////////////////////////////////////////////
		if (WLmapping->Enabled == false && wlmappingfinished == true) {
			WLmapping->Enabled = true;
			wlmappingfinished = false;
			label13->Text = L"Loader mapping done";
			label13->ForeColor = System::Drawing::Color::Green;
		}
		//////////////////////////////////////////////////////////////////////////////
//		ms0.getRs232cWLCtrl()->CheckStatus(false,false);
		ShowLoaderStatus();

		this->textBox2->Text = snstring;

		//////////////////////////////////////////////////////////////////////////////
		//ShowPictureJustTaken();


		nowTime = nowTime + 0.05;
		textTimer->Text = nowTime.ToString("0.0");
	}
	private: System::Void startBotton_Click(System::Object^  sender, System::EventArgs^  e) {
		ms0.SetClosePlot(true);
		std::cout << "start scanning... " << std::endl;
		emergencystop = false;
		scanfinished = false;

		label13->Text = L"Scanning";
		label13->ForeColor = System::Drawing::Color::Blue;
		startBotton->Enabled = false;
		ThreadStart^ threadDelegate = gcnew ThreadStart(this, &ControlGUI::FullTrayAutoScan);
		tscan = gcnew Thread(threadDelegate);
		tscan->IsBackground = true;
		tscan->Start();
		ms0.SetClosePlot(false);
	}
	private: System::Void singlescan_Click(System::Object^  sender, System::EventArgs^  e) {
		std::cout << "start scanning... " << std::endl;
//		ms0.SetClosePlot(false);
		emergencystop = false;
		scanfinished = false;
		label13->Text = L"Pattern Matching";
//		PatternMatching();
		label13->Text = L"Scanning";
		label13->ForeColor = System::Drawing::Color::Blue;
		startBotton->Enabled = false;
		ThreadStart^ threadDelegate = gcnew ThreadStart(this, &ControlGUI::ScanningSingle);
		tscan = gcnew Thread(threadDelegate);
		tscan->IsBackground = true;
		tscan->Start();

	}
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		if (comboBox1->Text == L"Strip sensor scan") {
			startx->Text = "-49000";
			starty->Text = "-49000";
			stepx->Text = "10000";
			stepy->Text = "10000";
			endx->Text = "49000";
			endy->Text = "49000";
			scanmark->SetSensorType("strip");
			DrawScanPosition();
		} 
		else if(comboBox1->Text == L"Strip sensor photo") {
			startx->Text = "-48975";
			starty->Text = "-48810";
			stepx->Text = "11700";
			stepy->Text = "7800";
			endx->Text = "48975";
			endy->Text = "48810";
			scanmark->SetSensorType("strip");
			DrawScanPosition();
		}
		else if (comboBox1->Text == L"Pixel sensor scan") {
			startx->Text = "0";
			starty->Text = "0";
			stepx->Text = "3000";
			stepy->Text = "3000";
			endx->Text = "40000";
			endy->Text = "40000";
			scanmark->SetSensorType("pixel");
			DrawScanPosition();
		}
		else if (comboBox1->Text == L"Simple Test") {
			startx->Text = "-10000";
			starty->Text = "-10000";
			stepx->Text = "5000";
			stepy->Text = "5000";
			endx->Text = "10000";
			endy->Text = "10000";
			scanmark->SetSensorType("test");
			DrawScanPosition();
		}
		else if (comboBox1->Text == L"Single Point") {
			startx->Text = "0";
			starty->Text = "0";
			stepx->Text = "1000";
			stepy->Text = "1000";
			endx->Text = "0";
			endy->Text = "0";
			scanmark->SetSensorType("point");
			DrawScanPosition();
		}
		else if (comboBox1->Text == L"Line Scan") {
			startx->Text = "-45000";
			starty->Text = "0";
			stepx->Text = "5000";
			stepy->Text = "5000";
			endx->Text = "45000";
			endy->Text = "0";
			scanmark->SetSensorType("line");
			DrawScanPosition();
		}
		else {
			startx->Text = "-49000";
			starty->Text = "-49000";
			stepx->Text = "10000";
			stepy->Text = "10000";
			endx->Text = "49000";
			endy->Text = "49000";
			scanmark->SetSensorType("strip");
			DrawScanPosition();

		}

	}
private: System::Void rbx2_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
//	ms0.getRs232cRVCtrl()->RVSet(2);
//	ms0.getRs232cAFCtrl()->SetZoom(2);
	ms0.SetZoom(2);
	Sleep(500);
	AutoFocus();
}


private: System::Void rbx5_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
//	ms0.getRs232cRVCtrl()->RVSet(5);
//	ms0.getRs232cAFCtrl()->SetZoom(5);
	ms0.SetZoom(5);
	Sleep(500);
	AutoFocus();
}
private: System::Void rbx10_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	//ms0.getRs232cRVCtrl()->RVSet(10);
	//ms0.getRs232cAFCtrl()->SetZoom(10);
	ms0.SetZoom(10);
	Sleep(500);
	AutoFocus();
}
private: System::Void stopButton_Click(System::Object^  sender, System::EventArgs^  e) {
	std::cout << "stop buton pushed" << std::endl;
	tscan->Abort();
	delete tscan;

	scanfinished = true;
	emergencystop = true;

}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	ms0.getRs232cXYCtrl()->m_prsMain->CloseCommPort();
	ms0.getRs232cAFCtrl()->m_prsMain->CloseCommPort();
	ms0.getRs232cRVCtrl()->m_prsMain->CloseCommPort();
	Close();
}

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	bool dothread = false;
	if (dothread) {
		ThreadStart^ threadDelegate = gcnew ThreadStart(this, &ControlGUI::AutoFocus);
		tsc0 = gcnew Thread(threadDelegate);
		tsc0->IsBackground = true;
		tsc0->Start();
	}else{
//		ms0.getRs232cAFCtrl()->SendSC0(1);
		AutoFocus();
	}
}


private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	ms0.getTurtleSwitch()->takepicture();
}


private: System::Void stageup_Click(System::Object^  sender, System::EventArgs^  e) {
	float move = float::Parse(stepval->Text);
	ms0.getRs232cXYCtrl()->RelativeM(2, move * 1000 / UMPARPULSEY);
	ms0.getRs232cXYCtrl()->PrintPosition();
	DrawScanPosition();
//	ms0.getRs232cAFCtrl()->CheckStatus();
//	Sleep(200);
//	ms0.getRs232cAFCtrl()->GetZPos();
}
private: System::Void stageleft_Click(System::Object^  sender, System::EventArgs^  e) {
	float move = float::Parse(stepval->Text);
	ms0.getRs232cXYCtrl()->RelativeM(1, move * 1000 / UMPARPULSEX);
	ms0.getRs232cXYCtrl()->PrintPosition();
	DrawScanPosition();
//	ms0.getRs232cAFCtrl()->CheckStatus();
//	Sleep(200);
//	ms0.getRs232cAFCtrl()->GetZPos();
}
private: System::Void stageright_Click(System::Object^  sender, System::EventArgs^  e) {
	float move = float::Parse(stepval->Text);
	ms0.getRs232cXYCtrl()->RelativeP(1, move * 1000 / UMPARPULSEX);
	ms0.getRs232cXYCtrl()->PrintPosition();
	DrawScanPosition();
//	ms0.getRs232cAFCtrl()->CheckStatus();
//	Sleep(200);
//	ms0.getRs232cAFCtrl()->GetZPos();
}
private: System::Void stagedown_Click(System::Object^  sender, System::EventArgs^  e) {
	float move = float::Parse(stepval->Text);
	ms0.getRs232cXYCtrl()->RelativeP(2, move * 1000 / UMPARPULSEY);
	ms0.getRs232cXYCtrl()->PrintPosition();
	DrawScanPosition();
//	ms0.getRs232cAFCtrl()->CheckStatus();
//	Sleep(200);
//	ms0.getRs232cAFCtrl()->GetZPos();
}
private: System::Void stageorig_Click(System::Object^  sender, System::EventArgs^  e) {
//	ms0.getRs232cXYCtrl()->Origin(1);
//	ms0.getRs232cXYCtrl()->Origin(2);
	ms0.getRs232cXYCtrl()->Absolute2D(SCANORIGPOSX, SCANORIGPOSY);
	ms0.getRs232cXYCtrl()->PrintPosition();
	DrawScanPosition();
//	ms0.getRs232cAFCtrl()->CheckStatus();
//	Sleep(200);
//	ms0.getRs232cAFCtrl()->GetZPos();
}


private: System::Void picture_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	int type = 0;
	if(this->picture->Checked)type=type|0b1;
	if(this->afocus->Checked)type=type|0b10;
	ms0.SetScanMode(type);
}
private: System::Void afocus_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	int type = 0;
	if (this->picture->Checked)type = type | 0b1;
	if (this->afocus->Checked)type = type | 0b10;
	std::cout << type << std::endl;
	ms0.SetScanMode(type);
}
private: System::Void closeplot_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	if (this->closeplot->Checked) ms0.SetClosePlot(true);
	else ms0.SetClosePlot(false);
}
private: System::Void movefar_Click(System::Object^  sender, System::EventArgs^  e) {
	MoveAFZpos('F', 50);

}
private: System::Void movenear_Click(System::Object^  sender, System::EventArgs^  e) {
	MoveAFZpos('N', 50);
}


private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	MoveTakeWaferPosition();
}

private: System::Void button4_Click_1(System::Object^  sender, System::EventArgs^  e) {
	ms0.getRs232cXYCtrl()->GoHome();
}

private: System::Void WLcheck_Click(System::Object^  sender, System::EventArgs^  e) {
//	label13->Text = L"Checking Status";
//	label13->ForeColor = System::Drawing::Color::Blue;
	if (ms0.getRs232cWLCtrl()->CheckStatus(false, true) != 0) ShowWaferLoaderErrors();
//	label13->Text = L"Ready";
}
		 void WaferLoaderInitialize() {
			 snstring = this->serialnumber->Text;
			 if (ms0.getRs232cWLCtrl()->Initialization() != 0) ShowWaferLoaderErrors();
			 wlinitfinished = true;

		 }
		 void WaferLoaderMapping() {
			 if (ms0.getRs232cWLCtrl()->doMapping() != 0) ShowWaferLoaderErrors();
			 wlmappingfinished = true;
		 }
private: System::Void WLinit_Click(System::Object^  sender, System::EventArgs^  e) {
	std::cout << "Loader initializing... " << std::endl;
	wlinitfinished = false;
	label13->Text = L"loader initializing...";
	label13->ForeColor = System::Drawing::Color::Blue;
	WLinit->Enabled = false;
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &ControlGUI::WaferLoaderInitialize);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();//	label13->Text = L"initializing wafer loader";


}
private: System::Void WLmapping_Click(System::Object^  sender, System::EventArgs^  e) {
	std::cout << "Loader Mapping... " << std::endl;
	wlmappingfinished = false;
	label13->Text = L"loader mapping...";
	label13->ForeColor = System::Drawing::Color::Blue;
	WLmapping->Enabled = false;
	ThreadStart^ threadDelegate = gcnew ThreadStart(this, &ControlGUI::WaferLoaderMapping);
	tscan = gcnew Thread(threadDelegate);
	tscan->IsBackground = true;
	tscan->Start();
}
private: System::Void setTray_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	std::string str = msclr::interop::marshal_as<std::string>(combsetTray->Text);
	traynum = atoi(str.substr(5).c_str());
	std::cout << "[" << str << "] " << traynum << std::endl;
}
		 void OneTrayLoad() {

			 MoveTakeWaferPosition();
			 //	label13->Text = L"Loading a wafer to Stage";
			 int ret = ms0.LoadOneTray(traynum);
			 //	label13->Text = L"Ready";
			 if (ret == -3) {
				 std::stringstream ss; ss.str("");
				 ss << "tray # " << traynum << " is not ready to load \n"
					 << "   status : " << ms0.getRs232cWLCtrl()->GetTS(traynum);

				 std::cout << ss.str() << std::endl;
				 System::String^ mess_str = gcnew System::String(ss.str().c_str());
				 System::Windows::Forms::MessageBox::Show(mess_str);
			 }
			 else if (ret == -2) {
				 std::cout << "stage is not at take wafer point" << std::endl;
				 System::Windows::Forms::MessageBox::Show("stage is not at take wafer point");
			 }
			 else if (ret == -1) ShowWaferLoaderErrors();
			 else std::cout << "tray # " << traynum << " successfully loaded" << std::endl;
		 }
private: System::Void load_Click(System::Object^  sender, System::EventArgs^  e) {
	OneTrayLoad();
}
		 void OneTrayUnload() {
			 MoveTakeWaferPosition();
			 label13->Text = L"Putting back to Cassette";
			 int ret = ms0.UnLoadOneTray(traynum);
			 label13->Text = L"Ready";
			 if (ret == -3) {
				 std::stringstream ss; ss.str("");
				 ss << "tray # " << traynum << " is not ready to load \n"
					 << "   status : " << ms0.getRs232cWLCtrl()->GetTS(traynum);

				 std::cout << ss.str() << std::endl;
				 System::String^ mess_str = gcnew System::String(ss.str().c_str());
				 System::Windows::Forms::MessageBox::Show(mess_str);
			 }

			 else if (ret == -2) {
				 std::cout << "stage is not at take wafer point" << std::endl;
				 System::Windows::Forms::MessageBox::Show("stage is not at take wafer point");
			 }
			 else if (ret == -1) ShowWaferLoaderErrors();
			 else std::cout << "tray # " << traynum << " successfully unloaded" << std::endl;
		 }
private: System::Void unload_Click(System::Object^  sender, System::EventArgs^  e) {
	OneTrayUnload();
}
private: System::Void vacon_Click(System::Object^ sender, System::EventArgs^ e) {
	ms0.VacuumControl(1);
}
private: System::Void vacoff_Click(System::Object^ sender, System::EventArgs^ e) {
		 ms0.VacuumControl(0);
}
};
}
