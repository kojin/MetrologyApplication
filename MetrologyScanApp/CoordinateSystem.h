#pragma once

#include <iostream>
#define MM 1000

// mini axis for test
//#define XAXISMIN -6000 // [pls]
//#define YAXISMIN -4000 // [pls]
//#define XAXISMAX 6000  // [pls]
//#define YAXISMAX 8000  // [pls]
//#define XAXISMIN -25000 // [pls]
#define XAXISMIN -50000 // -25000 [pls]
#define YAXISMIN -50000 // -38000 [pls]
#define XAXISMAX 50000  // 25000 [pls]
#define YAXISMAX 50000  // 38000 [pls]
#define UMPARPULSEX -4   // default -4um/pulse : can be modified.  Sign indicate the direction of axis to the stage direction
#define UMPARPULSEY -4   // default -4um/pulse : can be modified.  Sign indicate the direction of axis to the stage direction

#define UMPARPULSEAF 0.156 // default 0.156um/pulse : can be modified.

// wafer size & Orientation Flat
#define WAFERSIZE 152400 // [um] for 6 inch sensor
//#define ORIFLA 4076
#define ORIFLA 8000

#define TAKEWAFERPOSX +37619  // 37500 [pls]
#define TAKEWAFERPOSY -20311  // -20000 [pls]
#define SCANORIGPOSX 375  // 4500 [pls]
#define SCANORIGPOSY 5375  // -6500 [pls]
//#define SCANORIGPOSX 0  // [pls]
//#define SCANORIGPOSY 0  // [pls]
#define PIXELSIZE 3.9 // [um]/pixel
#define PHOTOH  4000 // pix
#define PHOTOW  6000 // pix


// Sensor die size
#define STRIPW 97950.5 // [um]
#define STRIPH 97621 // [um]
#define PIXELW 41100 // [um]
#define PIXELH 39500 // [mm]
#define PIXELG 1000 // [um]
#define PIXMIDY -3150 // [mm]

public ref class CoordinateSystem
{
private: float pictureH; // [mm] on GUI
private: float pictureW; // [mm] on GUI
private: float pictureSize;  // [mm] on GUI
private: float scanOrigX; // [um]
private: float scanOrigY; // [um]
private: float scanOrigdX;  // [um]
		 float scanOrigdY;  // [um]


public:
	CoordinateSystem() {};
	CoordinateSystem::CoordinateSystem(float _picH, float _picW, float scanorgx/*[pls]*/, float scanorgy/*[pls]*/);
	float xposmin;
	float yposmin;
	float xposmax;
	float yposmax;
	float scale;
	public:void setCoordination(float _picH, float _picW, float scanorgx, float scanorgy);
		   void setScanOrigCorrection(float dx, float dy) { scanOrigdX = dx; scanOrigdY = dy; }
	public: void getScanPosFromAxis(float xx, float yy, float * pos);
	public: void getPicturePosFromAxis(float xx, float yy, float * pos);	
	public: void getPicturePosFromScanPos(float aa, float bb, float * pos);
	public: void getAxisFromScanPos(float aa, float bb, float * pos);
	
};

