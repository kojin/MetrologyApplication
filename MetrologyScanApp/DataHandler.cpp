#include "stdafx.h"
#include "DataHandler.h"
#include <iostream>
#include <filesystem>
#include <sstream>

//namespace fs = std::experimental::filesystem;
namespace fs = std::filesystem;

DataHandler::DataHandler()
{
	 dirpath = "C:\\work\\ATLASproduction\\TempSpace";
}

void DataHandler::FileHandling() {

}
std::string DataHandler::GetSamplePictureName() {
	return dirpath + "\\pattern_templatepicture.jpg";
}
std::string DataHandler::GetLastPictureName() {
	std::string fname="";
	for (const auto & entry : fs::directory_iterator(dirpath)) {
		std::string filename = entry.path().string();
//		std::cout << filename << std::endl;
		if (filename.find(".jpg") + 4 == filename.length()) {
			fname = filename;
		}
	}
	if (fname != lastpicname) {
		lastpicname = fname;
		return  fname;
	}
	else return "SAME";
}
void DataHandler::MoveImageToDir(std::string newdir) {
	std::cout << "check directory " << dirpath << std::endl;
	int ii = 1;
	std::stringstream ss; 
	ss << newdir;
	while (fs::exists(dirpath + "\\" + ss.str())) {
		ss.str("");
		ss << newdir << "_" << ii;
		ii++;
	}
	newdir = ss.str();
	fs::create_directory(dirpath + "\\" + newdir);
	for (const auto & entry : fs::directory_iterator(dirpath)) {
		std::string filename = entry.path().string();
		std::cout << filename << std::endl;
		if (filename.find(".jpg") + 4 == filename.length()
			|| filename.find(".nef") + 4 == filename.length()
			|| filename.find(".png") + 4 == filename.length()
			|| filename.find(".pdf") + 4 == filename.length()
			|| filename.find(".root") + 5 == filename.length()
			) {
			fs::copy(filename, dirpath + "\\" + newdir);
			fs::remove(filename);
		}
	}
//	fs::create_directory(dirpath + "\\" + newdir);
	fs::copy("C:\\work\\ATLASproduction\\ROOTAnalyzer\\log.txt", dirpath + "\\" + newdir);
}