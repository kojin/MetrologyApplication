#pragma once
#include <Windows.h>
#include <string>

typedef struct {
	DWORD BaudRate;       /* Boud(ex 9600, 14400, ...) */
	BYTE ByteSize;        /* Number of bits/byte, 4-8        */
	BYTE Parity;          /* 0-4=None,Odd,Even,Mark,Space    */
	BYTE StopBits;        /* 0,1,2 = 1, 1.5, 2               */
} INIT_COMM_PAC;


class CRs232c
{
	///////////////////////////////////////////////////////////////////////////
//	Life Cycle

//	Birth
public:
	CRs232c();
	CRs232c(char*, int r_nInputBuffer = 2000, int r_nOutputBuffer = 2000);

	//	Death
	~CRs232c();

	///////////////////////////////////////////////////////////////////////////
	//	Operator

	///////////////////////////////////////////////////////////////////////////
	//	Data

	//	public data
public:

	//	protected data
protected:
	HANDLE m_hCom;									//
	HANDLE m_hRead;									//
	HANDLE m_hWrite;								//

	int m_nInputBuffer;								// Input buffer
	int m_nOutputBuffer;							// Output buffer

	//	private data
private:

	///////////////////////////////////////////////////////////////////////////
	//	Function

	//	public Function
public:
	BOOL OpenCommPort(char*, int r_nInputBuffer = 2000, int r_nOutputBuffer = 2000);
	void CloseCommPort();
	HANDLE *GetHComm() { return &m_hCom; }
	BOOL InitCommPort(INIT_COMM_PAC*);
	BOOL InitCommPort(std::string setting);
	BOOL InitCommPort2();

	int ReadComm(DWORD, LPSTR);
	int ReadCommBlock(LPSTR, int);

	BOOL WriteCommBlock(LPSTR, DWORD);


	//	protected Function
protected:

	//	private Function
private:

};

