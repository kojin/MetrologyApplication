#include "stdafx.h"
#include <iostream>
#include "ScanMarker.h"

using namespace MoveScanMarker;

ScanMarker::ScanMarker()
{
}
ScanMarker::ScanMarker(PictureBox^ pb, Bitmap^ cv, Brush^ cl, CoordinateSystem ^cs) {
	cs0 = cs;
	pictureBox = pb;
	canvas = cv;
	brushColor = cl;
	canvasSizeX = (float)pb->Width;
	canvasSizeY = (float)pb->Height;
	canvasSize = canvasSizeX > canvasSizeY ? canvasSizeY : canvasSizeX;
	radius = 10;
	pitchX = radius / 2;
	pitchY = radius / 2;
	directionX = +1;
	directionY = 0;
	sensortype = new std::string("");
	g = Graphics::FromImage(canvas);

//	Pen blackPen(Color(255, 0, 0, 0), 3);
//	g->DrawArc(&blackPen,canvasSizeX/2,canvasSizeY/2,canvasSize/2,canvasSize/2,-60,300);
//	Brush ^ bshGy = gcnew SolidBrush(Color::Gray);
	reddotPen = gcnew Pen(Color::DarkRed, 2);
	reddotPen->DashStyle = System::Drawing::Drawing2D::DashStyle::Dot;
	blackPen = gcnew Pen(Color::Black, 2);
}

void ScanMarker::DrawBase() {
	float posorig[2] = { 0.,0. };
	cs0->getPicturePosFromAxis(-1.*WAFERSIZE/2, -1.*WAFERSIZE/2, posorig);
	g->FillEllipse(Brushes::LightYellow, posorig[0], posorig[1], WAFERSIZE*cs0->scale, WAFERSIZE*cs0->scale);
	g->FillRectangle(Brushes::Black, cs0->scale*ORIFLA / 2, canvasSizeY - cs0->scale*ORIFLA / 2, canvasSizeX, canvasSizeY);
//	g->FillRectangle(Brushes::Black, cs0->scale*ORIFLA , canvasSizeY - cs0->scale*ORIFLA , canvasSizeX, canvasSizeY);
	DrawSingleLine(reddotPen, -0.25*WAFERSIZE, 0., 0.25*WAFERSIZE, 0.);
	DrawSingleLine(reddotPen, 0., -0.25*WAFERSIZE, 0., 0.25*WAFERSIZE);
	if(*sensortype=="strip"){
		DrawRectangleLines(blackPen, -0.5 * STRIPW * MM, -0.5 * STRIPH * MM, 0.5 * STRIPW * MM, 0.5 * STRIPH * MM);
	} 
	else if (*sensortype == "pixel") {
		float sizex = (float)PIXELW;
		float sizey = (float)PIXELH;
		float gap = (float)PIXELG;
		float midy = (float)PIXMIDY;
		float centx[6] = {0.,-1 * (sizex + gap),sizex + gap, 0.,-sizex/2 - gap/2,sizex/2 + gap/2 };
		float centy[6] = { midy, midy, midy, midy-sizey-gap,midy+sizey+gap,midy+sizey+gap};
		for (int ii = 0; ii < 6; ii++) {
			DrawRectangleLines(blackPen,
					(centx[ii] - sizex / 2)*MM,
					(centy[ii] - sizey / 2)*MM,
					(centx[ii] + sizex / 2)*MM,
					(centy[ii] + sizey / 2)*MM);
		}
	}
	else if (*sensortype == "test") {
		DrawRectangleLines(blackPen, XAXISMIN*UMPARPULSEX, YAXISMIN*UMPARPULSEY, XAXISMAX*UMPARPULSEX, YAXISMAX*UMPARPULSEY);
	}
}
void ScanMarker::DrawSingleLine(Pen^ pen,float x1,float y1,float x2, float y2) {
	float pos0[2] = { 0,0 };
	float pos1[2] = { 0.,0. };
	cs0->getPicturePosFromAxis(x1, y1, pos0);
	cs0->getPicturePosFromAxis(x2, y2, pos1);
	g->DrawLine(pen, pos0[0], pos0[1], pos1[0], pos1[1]);

}
void ScanMarker::DrawRectangleLines(Pen^ pen, float x1, float y1, float x2, float y2) {
	DrawSingleLine(pen, x1, y1, x1, y2);
	DrawSingleLine(pen, x1, y1, x2, y1);
	DrawSingleLine(pen, x2, y2, x1, y2);
	DrawSingleLine(pen, x2, y2, x2, y1);

}
/*
float* ScanMarker::getRelativePos(float xx, float yy, float *relpos) {
//	float relpos[2] = { 0,0 };
	float origposx = canvasSizeX / 2;
	float origposy = canvasSizeY / 2;
	float scale = 160 / canvasSize;

	relpos[0] = (xx - canvasSizeX)*scale;
	relpos[1] = (yy - canvasSizeY)*scale;
	return relpos;
}

float* ScanMarker::getPicturePos(float xx, float yy, float *picpos) {
//	float picpos[2] = { 0,0 };
	float scale = canvasSize / 160;
	xx *= scale; yy *= scale;
	xx += canvasSizeX / 2;
	yy += canvasSizeY / 2;
	picpos[0] = xx;
	picpos[1] = yy;
	return picpos;
}
*/
void ScanMarker::PutCircle(float aa, float bb) {
	float pos[2];
	cs0->getPicturePosFromScanPos(aa, bb, pos);
	positionX = pos[0];
	positionY = pos[1];
	{
		g->FillEllipse(brushColor, positionX-radius, positionY-radius, radius * 2, radius * 2);
		pictureBox->Image = canvas;
	}

}
void ScanMarker::ChagePreviousCircle() {
	if (previousX == 0) {
		previousX = canvasSizeX/2;
	}
	if (previousY == 0) {
		previousY = canvasSizeY/2;
	}
	{
		g->FillEllipse(Brushes::LightYellow, previousX-radius, previousY-radius, radius * 2, radius * 2);
		pictureBox->Image = canvas;
		previousX = positionX;
		previousY = positionY;
		DrawBase();
		//		g->DrawLine(redPen, 0., canvasSizeY / 2 + cs0->scale*ORIFLA / 2, canvasSizeX, canvasSizeY / 2 + cs0->scale*ORIFLA / 2);
//		g->DrawLine(redPen, canvasSizeX / 2, 0., canvasSizeX / 2, canvasSizeY);
	}
}
