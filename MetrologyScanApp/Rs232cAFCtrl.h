#pragma once
#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include "Rs232c.h"
#include "CoordinateSystem.h"

#define		LF			0x0a
#define		CR			0x0d
#define		NUL			0x00
#define		ErrRetry	3000


const char pszCEMessage[] = "Command Error";
const char pszFEMessage[] = "Far Error";
const char pszPEMessage[] = "Peak Error";
const char pszJMessage[] = "Just Focus";
const char pszJFMessage[] = "Just Focus (Far)";
const char pszJNMessage[] = "Just Focus (Near)";
const char pszCE[] = { 'C', 'E', 0x0d, 0x0a, '\0' };
const char pszFE[] = { 'F', 'E', 0x0d, 0x0a, '\0' };
const char pszPE[] = { 'P', 'E', 0x0d, 0x0a, '\0' };
const char pszJ[] = { 'J', 0x0d, 0x0a, '\0' };
const char pszJF[] = { 'J', 'F', 0x0d, 0x0a, '\0' };
const char pszJN[] = { 'J', 'N', 0x0d, 0x0a, '\0' };

class Rs232cAFCtrl
{
private:
	DCB		dcb;								// RS-232C Port
	COMSTAT	ComStat;
	int		IdCom, comerr;
	char	m_strCmd[20];                       // Send Character buffer


	void 	WriteMsc(void);					// Send MSC
public:
	Rs232cAFCtrl();
	~Rs232cAFCtrl();

	char	m_strResp[50];                      // Recive character buffer
	char	m_strDummy[5];                      // Recive character buffer
	CRs232c *m_prsMain;							// RS-232C Class
	int		RS_Setup(int m_nPort);			// RS Initialize
	void	RS_Reset(void);					// RS Close
	int		ReadMsc(char *m_strResp, unsigned int pt);
	void	WriteStringMsc(char *cCmd);		// Send string MSC
	int SendSC0(int n);
	int MoveZpos(char dir, int pulse);
	std::string CheckZoom();
	int SetZoom(int x);
	char * CheckStatus();
	int GetZPos();
	int ClearAF();
	int ResumeAF();
	int    AFTest();
	int nRcnt, nlen;
	char m_AFStatus[20];
	char m_AFZoom[20];
	int m_AFZpos;
	float m_AFZposum;
};

