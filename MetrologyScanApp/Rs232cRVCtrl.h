#pragma once


#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include "Rs232c.h"

#define		LF			0x0a
#define		CR			0x0d
#define		NUL			0x00
#define		ErrRetry	3000
#define     QU          0x3F

class Rs232cRVCtrl
{
	DCB		dcb;								// RS-232C Port
	COMSTAT	ComStat;
	int		IdCom, comerr;
	char	m_strCmd[20];                       // Send Character buffer

	void 	WriteMsc(void);					// Send MSC

public:
	Rs232cRVCtrl();
	~Rs232cRVCtrl();
	char	m_strResp[50];                      // Recive character buffer
	char	m_strDummy[5];                      // Recive character buffer
	CRs232c *m_prsMain;							// RS-232C Class
	int		RS_Setup(int m_nPort);			// RS Initialize
	void	RS_Reset(void);					// RS Close
	int		ReadMsc(char *m_strResp, unsigned int pt);
	int		ReadMsc(char *m_strResp);
	void	WriteStringMsc(char *cCmd);		// Send string MSC
	int    RVTest();
	int   RVSet(int zoom);

};

