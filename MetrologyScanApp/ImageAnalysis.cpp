#include "stdafx.h"
#include "ImageAnalysis.h"

/*
using namespace cv;
using namespace std;
ImageAnalysis::ImageAnalysis()
{
	std::cout << "Initializing ImageAnalysis Tool" << std::endl;
	grayscale = true;
	if (grayscale) {
		temp_image = imread("C:\\work\\ATLASproduction\\TempSpace\\pattern_template\\topleft.jpg", IMREAD_GRAYSCALE);
		//	temp_image = imread("C:\\work\\ATLASproduction\\TempSpace\\pattern_template\\topleft.jpg", IMREAD_GRAYSCALE);

	}
	else {
		temp_image = imread("C:\\work\\ATLASproduction\\TempSpace\\pattern_template\\topleft.jpg");
		//	temp_image = imread("C:\\work\\ATLASproduction\\TempSpace\\pattern_template\\topleft.jpg");
	}


}
void ImageAnalysis::SetTemplate(std::string position) {
	std::string temp_filename = "C:\\work\\ATLASproduction\\TempSpace\\pattern_template\\" + position + ".jpg";
	if (grayscale) {
		temp_image = imread(temp_filename.c_str(), IMREAD_GRAYSCALE);
		//	temp_image = imread("C:\\work\\ATLASproduction\\TempSpace\\pattern_template\\topleft.jpg", IMREAD_GRAYSCALE);

	}
	else {
		temp_image = imread(temp_filename.c_str());
		//	temp_image = imread("C:\\work\\ATLASproduction\\TempSpace\\pattern_template\\topleft.jpg");
	}
}

void ImageAnalysis::GetCornerPosition(std::string filename,  int*pos) {


//	cv::Mat img(cv::Size(320, 240), CV_8UC3, cv::Scalar(0, 0, 255));

//	Mat src_image = imread("C:\\work\\ATLASproduction\\TempSpace\\test_57\\Img0276.jpg");
	Mat src_image = imread(filename.c_str());
	Mat bin_image;
	if (grayscale) {
		cvtColor(src_image, bin_image, CV_BGR2GRAY);
	}
	else 
	{
		bin_image = src_image;
	}



	std::cout << "matching " << std::endl;
	Mat result;
//	matchTemplate(bin_image, temp_image, result, TM_CCORR_NORMED);
	matchTemplate(bin_image, temp_image, result,3);

	double ComparePictures;
//	ComparePictures = cv:: matchShapes(bin_image, result, CV_CONTOURS_MATCH_I1, 0);
//	ComparePictures = cv::matchShapes(bin_image, result, CV_CONTOURS_MATCH_I2, 0);
	ComparePictures = cv::matchShapes(bin_image, result, CV_CONTOURS_MATCH_I3, 0);
	std::cout <<"similarity="<< ComparePictures << std::endl;



//	cv::Mat image = cv::Mat::zeros(100, 100, CV_8UC3);	
//	imshow("", image);
//	namedWindow("img", CV_WINDOW_NORMAL);
//	namedWindow("img", );
	//	resize("img", result, cv::Size(), 1, 1);
//	imshow("img", result);
	
	
//	waitKey(0);
	
	Point maxPt;
	minMaxLoc(result, 0, 0, 0, &maxPt);
	rectangle(src_image, maxPt, Point(maxPt.x + temp_image.cols, maxPt.y + temp_image.rows), 2, 8, 0);
	std::cout << "matching position (x,y)=("<< maxPt.x +100 << ", " << maxPt.y +100 << ")" << std::endl;

	pos[0] = maxPt.x + 100;
	pos[1] = maxPt.y + 100;
//	namedWindow("img",WINDOW_NORMAL);
//	namedWindow("img", CV_WINDOW_NORMAL);
//	imshow("img", src_image);


		
}

*/
