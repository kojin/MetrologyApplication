/*+-------------------------------------------------------------------------------------+*/
/*|	Class		�FCRs232c																|*/
/*|	Contebts	�FRS232c control														|*/
/*+-------------------------------------------------------------------------------------+*/
#include	"stdafx.h"
#include	<string.h>
#include	<stdio.h>
#include	<stdlib.h>
#include <iostream>

#include	"Rs232c.h"

///////////////////////////////////////////////////////////////////////////////////////////
//		Constructor
//
CRs232c::CRs232c()
{
	m_hCom = NULL;
	m_hRead = NULL;
	m_hWrite = NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////
//		Constructor2
//		With serial port open
//
CRs232c::CRs232c
(char * r_szName,
	int  r_nInputBuffer,
	int  r_nOutputBuffer
)
{
	m_hCom = NULL;
	m_hRead = NULL;
	m_hWrite = NULL;

	OpenCommPort(r_szName, r_nInputBuffer, r_nOutputBuffer);
}

///////////////////////////////////////////////////////////////////////////////////////////
//		Destructor
//
CRs232c::~CRs232c()
{
}

///////////////////////////////////////////////////////////////////////////////////////////
//		operator


///////////////////////////////////////////////////////////////////////////////////////////
//		Functions

///////////////////////////////////////////////////////////////////////////////////////////
//		Serial port open
//
BOOL CRs232c::OpenCommPort							// TRUE:sucsess FALSE:failed
(char* r_szName,								// Port name
	int  r_nInputBuffer,						// Input buffer
	int  r_nOutputBuffer						// Output buffer
)
{
	DWORD	dwError;

	// already open
	if (m_hCom)return TRUE;

	/* open */
	m_hCom = CreateFileA(
		r_szName,			/* Port name */
		GENERIC_READ | GENERIC_WRITE,		/* Read/Write */
		0,								/* Common mode */
		NULL,							/* No security */
		OPEN_EXISTING,					/* Constant */
		FILE_ATTRIBUTE_NORMAL,
//		FILE_FLAG_OVERLAPPED,			/* Overlapped I/O */
		NULL);							/* Constant */

	//std::cout << r_szName << " " << std::hex << GENERIC_READ << " " << GENERIC_WRITE << " " << OPEN_EXISTING << " " << FILE_FLAG_OVERLAPPED << std::dec<< std::endl;
	//std::cout << m_hCom << " " << INVALID_HANDLE_VALUE << std::endl;
	if (m_hCom == INVALID_HANDLE_VALUE) {
		dwError = GetLastError();
		printf( "\n<OpenCommPort-%u>  : CreateFile() Error", dwError );
		return(FALSE);
	}

	/* Create ReadCommBlock event*/
	m_hRead = CreateEventA(NULL,				/* No security */
		FALSE, 				/* Auto reset */
		FALSE, 				/* Initial signal state */
		"Rs232cToRead");	/* Event name */

	if (m_hRead == NULL) {
		dwError = GetLastError();
		//		printf( "\n<OpenCommPort-%u>  : CreateEvent() Error", dwError );
		return(FALSE);
	}

	/* Create WriteCommBlock event */
	m_hWrite = CreateEventA(NULL,				/* No security */
		FALSE, 				/* Auto reset */
		FALSE, 				/* Initial signal state */
		"Rs232cToWrite");	/* Event name */

	if (m_hWrite == NULL) {
		dwError = GetLastError();
		//		printf( "\n<OpenCommPort-%u>  : CreateEvent() Error", dwError );
		return(FALSE);
	}

	// Input/Output buffer
	m_nInputBuffer = r_nInputBuffer;
	m_nOutputBuffer = r_nOutputBuffer;

	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////
//		Serial port close
//
void CRs232c::CloseCommPort()
{
	if (m_hCom) {
		CloseHandle(m_hCom);
		CloseHandle(m_hRead);
		CloseHandle(m_hWrite);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
//		Initialize Serial port
//
BOOL CRs232c::InitCommPort							//TRUE:normal FALSE:failed
(INIT_COMM_PAC *r_pac						//Initialize parameter pointer
)
/*+-------------------------------------------------------------------------------------+*/
{
	COMMTIMEOUTS	CommTimeOuts;			/* Connect time out structure */
	DCB				CommDcb;				/* Device control block structure */
	DWORD			dwError;

	/* Communication mask */
	if (!SetCommMask(m_hCom,				/* COM handle */
		EV_RXCHAR)) {					/* recieved character */
		dwError = GetLastError();
		//		printf( "\n<InitCommPort-%u>  : SetCommMask() Error", dwError );
		return(FALSE);
	}

	/* Input/Output buffer setting */
#if 1
	if (!SetupComm(m_hCom,					/* COM handle */
		2000L,							/* Input buffer size */
		2000L)) {						/* Output buffer size */
#else
	if (!SetupComm(m_hCom,					// COM handle
		m_nInputBuffer,					// Input buffer size
		m_nOutputBuffer)) {			// Output buffer size
#endif
		dwError = GetLastError();
		return(FALSE);
	}

	/* Time out setting */
	CommTimeOuts.ReadIntervalTimeout = 0xffffffff;
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 0;
	CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
	CommTimeOuts.WriteTotalTimeoutConstant = 20000;
	if (!SetCommTimeouts(m_hCom,			/* COM handle */
		&CommTimeOuts)) {				/* Connection time out suructure */
		dwError = GetLastError();
		//		printf( "\n<InitCommPort-%u>  : SetCommTimeOuts() Error", dwError );
		return(FALSE);
	}

	/* Read connection condition */
	GetCommState(m_hCom,					/* COM handle */
		&CommDcb);				/* Device control block structure */

/* Write connection condition */
	CommDcb.DCBlength = sizeof(DCB);		/* DCB structure size */
	CommDcb.BaudRate = r_pac->BaudRate;	/* Speed */
	CommDcb.fBinary = TRUE;				/* Binary mode */
	CommDcb.fParity = r_pac->Parity;				/* Parity check */
	CommDcb.ByteSize = r_pac->ByteSize;	/* Bit length */
	CommDcb.Parity = r_pac->Parity;		/* Parity */
	CommDcb.StopBits = r_pac->StopBits;	/* Stop bit */
	int ii = SetCommState(m_hCom,				/* COM handle */
		&CommDcb);
	if (!ii) {					/* Device control block structure */

		int ii;
		dwError = GetLastError();
		printf( "\n<InitCommPort-%u>  : SetCommState() Error", dwError );
		return(FALSE);
	}

	return(TRUE);
}

BOOL CRs232c::InitCommPort2()							//TRUE:normal FALSE:failed
 {
	COMMCONFIG cc;
	DCB dcb;
	COMMTIMEOUTS ct;
	DWORD			dwError;

	/*Initialize variable "cc"*/
	cc.dwSize = sizeof(COMMCONFIG);
	cc.wVersion = 1;

	/*Get Comm Port Status*/
	if (!GetCommState(m_hCom, &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : GetCommState() Error", dwError);
		return(FALSE);
	}

	/* Build Comm DCB */
//	if (!BuildCommDCBA("96,n,8,2,x",&dcb)) {
	if (FALSE == BuildCommDCBA("baud=9600 parity=n data=8 stop=1 xon=on", &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : BuildCommDCB() Error", dwError);
		return(FALSE);
	}

	/* Set Comm Port Status */
	if (!SetCommState(m_hCom, &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : SetCommState() Error", dwError);
		return(FALSE);

	}

	/* Get TimeoutSetting */
	if (!GetCommTimeouts(m_hCom, &ct)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : GetCommTimeouts() Error", dwError);
		return(FALSE);

	}
	/* Set COMMTIOMEOUT Structure */
	ct.ReadIntervalTimeout = 5;
	ct.WriteTotalTimeoutMultiplier = 5;
	ct.WriteTotalTimeoutConstant = 50;
	if (!SetCommTimeouts(m_hCom, &ct)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : SetCommTimeouts() Error", dwError);
		return(FALSE);

	}


	return(TRUE);

}
BOOL CRs232c::InitCommPort(std::string setting)							//TRUE:normal FALSE:failed
 {
	COMMCONFIG cc;
	DCB dcb;
	COMMTIMEOUTS ct;
	DWORD			dwError;

	/*Initialize variable "cc"*/
	cc.dwSize = sizeof(COMMCONFIG);
	cc.wVersion = 1;

	/*Get Comm Port Status*/
	if (!GetCommState(m_hCom, &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : GetCommState() Error", dwError);
		return(FALSE);
	}

	/* Build Comm DCB */
//	if (!BuildCommDCBA("96,n,8,2,x",&dcb)) {
//	if (FALSE == BuildCommDCBA("baud=19200 parity=n data=8 stop=2 xon=on", &dcb)) {
//	if (FALSE == BuildCommDCBA("baud=38400 parity=n data=8 stop=1 xon=off", &dcb)) {
	if (FALSE == BuildCommDCBA(setting.c_str(), &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : BuildCommDCB() Error", dwError);
		return(FALSE);
	}

	/* Set Comm Port Status */
	if (!SetCommState(m_hCom, &dcb)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : SetCommState() Error", dwError);
		return(FALSE);

	}

	/* Get TimeoutSetting */
	if (!GetCommTimeouts(m_hCom, &ct)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : GetCommTimeouts() Error", dwError);
		return(FALSE);

	}
	/* Set COMMTIOMEOUT Structure */
	ct.ReadIntervalTimeout = 5;
	ct.WriteTotalTimeoutMultiplier = 5;
	ct.WriteTotalTimeoutConstant = 50;
	if (!SetCommTimeouts(m_hCom, &ct)) {
		dwError = GetLastError();
		printf("\n<InitCommPort-%u>  : SetCommTimeouts() Error", dwError);
		return(FALSE);

	}


	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////
//		1 Byte read from Serial port
//
int CRs232c::ReadComm								//0:Normal -1:failed -2:time out
(DWORD dwTimeOut,							//Time out value(ms)
	LPSTR lpszBlock								// Read area
)
/*+-------------------------------------------------------------------------------------+*/
{
	DWORD	i;
	char szOutput[2];
	szOutput[1] = '\0';

	for (i = 0; i < dwTimeOut; i += 10) {	/* between 10ms */
		if (ReadCommBlock( /*lpszBlock*/szOutput, 1)) {
			*lpszBlock = szOutput[0];
			break;
		}
		Sleep(10);		/* sleep 10ms */
	}

	return(i < dwTimeOut ? 0 : -2);
}

///////////////////////////////////////////////////////////////////////////////////////////
//		Read from Serial port
//
int CRs232c::ReadCommBlock							//Real read size
(LPSTR lpszBlock,							//read area
	int nMaxLength								//Max read size
)
/*+-------------------------------------------------------------------------------------+*/
{
	BOOL       fReadStat;
	COMSTAT    ComStat;
	DWORD      dwErrorFlags;
	DWORD      dwLength;
	DWORD      dwError;
	OVERLAPPED	Overlapped;

	Overlapped.hEvent = m_hRead;
	Overlapped.Offset = 0;
	Overlapped.OffsetHigh = 0;

	// only try to read number of bytes in queue 
	ClearCommError(m_hCom, &dwErrorFlags, &ComStat);
	dwLength = min((DWORD)nMaxLength, ComStat.cbInQue);
//	std::cout << "dwLength = " << dwLength << " min : " << nMaxLength << " " << ComStat.cbInQue << std::endl;
	if (dwLength > 0)
	{
		fReadStat = ReadFile(m_hCom, lpszBlock,
			dwLength, &dwLength, &Overlapped);
		if (!fReadStat) {
			dwError = GetLastError();
			if (dwError == ERROR_IO_PENDING) {
				// We have to wait for read to complete. 
				// This function will timeout according to the
				// CommTimeOuts.ReadTotalTimeoutConstant variable
				// Every time it times out, check for port errors
				while (!GetOverlappedResult(m_hCom,
					&Overlapped, &dwLength, TRUE)) {
					dwError = GetLastError();
					if (dwError == ERROR_IO_INCOMPLETE)
						// normal result if not finished
						continue;
					else {
						//						printf( "\n<ReadCommBlock-%u> GetOverlappedResult() Error", dwError );
						break;
					}
				}

			}
			else {
				// some other error occurred
				dwLength = 0;
				ClearCommError(m_hCom, &dwErrorFlags, &ComStat);
				if (dwErrorFlags > 0) {
					//					printf( "<ReadCommBlock-%u> ReadFile() Error", dwErrorFlags ) ;
				}
			}
		}
	}

	return (dwLength);
}

///////////////////////////////////////////////////////////////////////////////////////////
//		Write to Serial port
//
BOOL CRs232c::WriteCommBlock						//TRUE:normal  FALSE:failed
(LPSTR lpByte,								//Write area
	DWORD dwBytesToWrite						//Write data size
)
{
	BOOL		fWriteStat;
	DWORD		dwBytesWritten;
	DWORD		dwErrorFlags;
	DWORD		dwError;
	COMSTAT 	ComStat;
	OVERLAPPED	Overlapped;

	if (!m_hCom)return FALSE;
	Overlapped.hEvent = m_hWrite;
	Overlapped.Offset = 0;
	Overlapped.OffsetHigh = 0;

	fWriteStat = WriteFile(m_hCom, lpByte, dwBytesToWrite,
		&dwBytesWritten, &Overlapped);

	// Note that normally the code will not execute the following
	// because the driver caches write operations. Small I/O requests 
	// (up to several thousand bytes) will normally be accepted 
	// immediately and WriteFile will return true even though an
	// overlapped operation was specified

	if (!fWriteStat) {
		dwError = GetLastError();
		if (dwError == ERROR_IO_PENDING) {
			// We should wait for the completion of the write operation
			// so we know if it worked or not

			// This is only one way to do this. It might be beneficial to 
			// the to place the writing operation in a separate thread 
			// so that blocking on completion will not negatively 
			// affect the responsiveness of the UI

			// If the write takes long enough to complete, this 
			// function will timeout according to the
			// CommTimeOuts.WriteTotalTimeoutConstant variable.
			// At that time we can check for errors and then wait 
			// some more.

			while (!GetOverlappedResult(m_hCom,
				&Overlapped, &dwBytesWritten, TRUE)) {
				dwError = GetLastError();
				if (dwError == ERROR_IO_INCOMPLETE)
					// normal result if not finished
					continue;
				else {
					// an error occurred, try to recover
//					printf( "\n<WriteCommBlock-%u>", dwError ) ;
					ClearCommError(m_hCom, &dwErrorFlags, &ComStat);
					if (dwErrorFlags > 0)
						//						printf( " <WriteCommBlock-%u>", dwErrorFlags ) ;
						break;
				}
			}
		}
		else {
			// some other error occurred
			ClearCommError(m_hCom, &dwErrorFlags, &ComStat);
			if (dwErrorFlags > 0)
				//				printf( "\n<WriteCommBlock-%u>", dwErrorFlags ) ;
				return (FALSE);
		}
	}
	return (TRUE);
}