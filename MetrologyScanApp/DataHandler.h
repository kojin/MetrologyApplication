#pragma once
#include <string>
#include <iostream>

class DataHandler
{
	std::string dirpath;
	std::string lastpicname;
public:
	DataHandler();
	~DataHandler() { ; }
	std::string GetDataDirPath() { return dirpath; }
	void FileHandling();
	void MoveImageToDir(std::string newdir);
	std::string GetLastPictureName();
	std::string GetSamplePictureName();
};

